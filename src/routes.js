import React from "react";
import rol from "./rol";
import CambioClaveForm from "./views/usuario/CambioClaveForm";

const Toaster = React.lazy(() =>
  import("./views/notifications/toaster/Toaster")
);
const Tables = React.lazy(() => import("./views/base/tables/Tables"));

const Breadcrumbs = React.lazy(() =>
  import("./views/base/breadcrumbs/Breadcrumbs")
);
const Cards = React.lazy(() => import("./views/base/cards/Cards"));
const Carousels = React.lazy(() => import("./views/base/carousels/Carousels"));
const Collapses = React.lazy(() => import("./views/base/collapses/Collapses"));
const BasicForms = React.lazy(() => import("./views/base/forms/BasicForms"));

const Jumbotrons = React.lazy(() =>
  import("./views/base/jumbotrons/Jumbotrons")
);
const ListGroups = React.lazy(() =>
  import("./views/base/list-groups/ListGroups")
);
const Navbars = React.lazy(() => import("./views/base/navbars/Navbars"));
const Navs = React.lazy(() => import("./views/base/navs/Navs"));
const Paginations = React.lazy(() =>
  import("./views/base/paginations/Pagnations")
);
const Popovers = React.lazy(() => import("./views/base/popovers/Popovers"));
const ProgressBar = React.lazy(() =>
  import("./views/base/progress-bar/ProgressBar")
);
const Switches = React.lazy(() => import("./views/base/switches/Switches"));

const Tabs = React.lazy(() => import("./views/base/tabs/Tabs"));
const Tooltips = React.lazy(() => import("./views/base/tooltips/Tooltips"));
const BrandButtons = React.lazy(() =>
  import("./views/buttons/brand-buttons/BrandButtons")
);
const ButtonDropdowns = React.lazy(() =>
  import("./views/buttons/button-dropdowns/ButtonDropdowns")
);
const ButtonGroups = React.lazy(() =>
  import("./views/buttons/button-groups/ButtonGroups")
);
const Buttons = React.lazy(() => import("./views/buttons/buttons/Buttons"));
const Charts = React.lazy(() => import("./views/charts/Charts"));
const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));
const CoreUIIcons = React.lazy(() =>
  import("./views/icons/coreui-icons/CoreUIIcons")
);
const Flags = React.lazy(() => import("./views/icons/flags/Flags"));
const Brands = React.lazy(() => import("./views/icons/brands/Brands"));
const Alerts = React.lazy(() => import("./views/notifications/alerts/Alerts"));
const Badges = React.lazy(() => import("./views/notifications/badges/Badges"));
const Modals = React.lazy(() => import("./views/notifications/modals/Modals"));
const Colors = React.lazy(() => import("./views/theme/colors/Colors"));
const Typography = React.lazy(() =>
  import("./views/theme/typography/Typography")
);
const Widgets = React.lazy(() => import("./views/widgets/Widgets"));
const Users = React.lazy(() => import("./views/users/Users"));
// Usuarios
const Registro = React.lazy(() => import("./views/usuario/Registro"));
const Usuarios = React.lazy(() => import("./views/usuario/Usuarios"));
const Editar = React.lazy(() => import("./views/usuario/Editar"));

const User = React.lazy(() => import("./views/users/User"));

const TipoParteForm = React.lazy(() =>
  import("./views/catalogos/tipoparte/TipoParteForm")
);
const PersonaForm = React.lazy(() =>
  import("./views/expediente/persona/PersonaForm")
);
const ExpedienteForm = React.lazy(() =>
  import("./views/expediente/ExpedienteForm")
);
const Expediente = React.lazy(() => import("./views/expediente/Expediente"));
const PruebaForm = React.lazy(() =>
  import("./views/expediente/prueba/PruebaForm")
);
const ExpedienteList = React.lazy(() =>
  import("./views/expediente/ExpedienteList")
);
const PersonaList = React.lazy(() =>
  import("./views/expediente/persona/PersonaList")
);
const DireccionesPersona = React.lazy(() =>
  import("./views/expediente/persona/DireccionesPersona")
);

const ExpedienteDetalle = React.lazy(() =>
  import("./views/expediente/ExpedienteDetalle")
);

const FaltaParteForm = React.lazy(() =>
  import("./views/expediente/faltaparte/FaltaParteForm")
);
const FaltaParteList = React.lazy(() =>
  import("./views/expediente/faltaparte/FaltaParteList")
);

const ExpedienteCaratula = React.lazy(() =>
  import("./views/expediente/ExpedienteCaratula")
);

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/theme", name: "Theme", component: Colors, exact: true },
  { path: "/theme/colors", name: "Colors", component: Colors },
  { path: "/theme/typography", name: "Typography", component: Typography },
  { path: "/base", name: "Base", component: Cards, exact: true },
  { path: "/base/breadcrumbs", name: "Breadcrumbs", component: Breadcrumbs },
  { path: "/base/cards", name: "Cards", component: Cards },
  { path: "/base/carousels", name: "Carousel", component: Carousels },
  { path: "/base/collapses", name: "Collapse", component: Collapses },
  { path: "/base/forms", name: "Forms", component: BasicForms },
  { path: "/base/jumbotrons", name: "Jumbotrons", component: Jumbotrons },
  { path: "/base/list-groups", name: "List Groups", component: ListGroups },
  { path: "/base/navbars", name: "Navbars", component: Navbars },
  { path: "/base/navs", name: "Navs", component: Navs },
  { path: "/base/paginations", name: "Paginations", component: Paginations },
  { path: "/base/popovers", name: "Popovers", component: Popovers },
  { path: "/base/progress-bar", name: "Progress Bar", component: ProgressBar },
  { path: "/base/switches", name: "Switches", component: Switches },
  { path: "/base/tables", name: "Tables", component: Tables },
  { path: "/base/tabs", name: "Tabs", component: Tabs },
  { path: "/base/tooltips", name: "Tooltips", component: Tooltips },
  { path: "/buttons", name: "Buttons", component: Buttons, exact: true },
  { path: "/buttons/buttons", name: "Buttons", component: Buttons },
  {
    path: "/buttons/button-dropdowns",
    name: "Dropdowns",
    component: ButtonDropdowns,
  },
  {
    path: "/buttons/button-groups",
    name: "Button Groups",
    component: ButtonGroups,
  },
  {
    path: "/buttons/brand-buttons",
    name: "Brand Buttons",
    component: BrandButtons,
  },
  { path: "/charts", name: "Charts", component: Charts },
  { path: "/icons", exact: true, name: "Icons", component: CoreUIIcons },
  { path: "/icons/coreui-icons", name: "CoreUI Icons", component: CoreUIIcons },
  { path: "/icons/flags", name: "Flags", component: Flags },
  { path: "/icons/brands", name: "Brands", component: Brands },
  {
    path: "/notifications",
    name: "Notifications",
    component: Alerts,
    exact: true,
  },
  { path: "/notifications/alerts", name: "Alerts", component: Alerts },
  { path: "/notifications/badges", name: "Badges", component: Badges },
  { path: "/notifications/modals", name: "Modals", component: Modals },
  { path: "/notifications/toaster", name: "Toaster", component: Toaster },
  { path: "/widgets", name: "Widgets", component: Widgets },
  { path: "/users", exact: true, name: "Users", component: Users },
  {
    path: "/usuarios/registro",
    exact: true,
    name: "Registro",
    component: Registro,
    roles: [rol.admin],
  },
  {
    path: "/usuarios/editar/:id",
    exact: true,
    name: "Editar usuario",
    component: Editar,
    roles: [rol.admin],
  },
  {
    path: "/usuarios/cambioclave",
    exact: true,
    name: "Cambiar clave de usuario",
    component: CambioClaveForm,
  },
  { path: "/usuarios", exact: true, name: "Usuarios", component: Usuarios },
  { path: "/usuarios/:id", exact: true, name: "User Details", component: User },

  {
    path: "/catalogo/tipoparte/form",
    exact: true,
    name: "TipoParteForm",
    component: TipoParteForm,
    roles: [rol.admin],
  },
  {
    path: "/expediente/persona/form",
    exact: true,
    name: "PersonaForm",
    component: PersonaForm,
  },
  {
    path: "/expediente/expedienteForm",
    exact: true,
    name: "ExpedienteForm",
    component: ExpedienteForm,
  },
  {
    path: "/expediente/expediente",
    exact: true,
    name: "Expediente",
    component: Expediente,
  },
  {
    path: "/expediente/prueba/pruebaform",
    exact: true,
    name: "PruebaForm",
    component: PruebaForm,
  },

  {
    path: "/expediente/expedientelist",
    exact: true,
    name: "ExpedienteList",
    component: ExpedienteList,
  },
  {
    path: "/expediente/persona/list",
    exact: true,
    name: "PersonaList",
    component: PersonaList,
  },

  {
    path: "/expediente/persona/direcciones/:idparte",
    exact: true,
    name: "DireccionesPersona",
    component: DireccionesPersona,
  },

  {
    path: "/expediente/detalle/:idexpediente",
    exact: true,
    name: "ExpedienteDetalle",
    component: ExpedienteDetalle,
  },

  {
    path: "/expediente/falta/form/:idparte",
    exact: true,
    name: "FaltaParteForm",
    component: FaltaParteForm,
  },
  {
    path: "/expediente/falta/list",
    exact: true,
    name: "FaltaParteList",
    roles: [rol.parte],
    component: FaltaParteList,
  },

  {
    path: "/expediente/caratulaexpediente/:idexpediente",
    exact: true,
    name: "ExpedienteCaratula",
    component: ExpedienteCaratula,
  },
];

export default routes;
