import React from "react";
import "./scss/style.scss";
import { HashRouter, Route, Switch } from "react-router-dom";
import useToken from "./components/useToken";
import ProtectedRoute from "./components/ProtectedRoute";
import rol from "./rol";

// Containers
const TheLayout = React.lazy(() => import("./containers/TheLayout"));

// Pages
const Register = React.lazy(() => import("./views/pages/register/Register"));
const Page404 = React.lazy(() => import("./views/pages/page404/Page404"));
const Page500 = React.lazy(() => import("./views/pages/page500/Page500"));
const Login = React.lazy(() => import("./views/pages/login/Login"));

function App() {
  const { token, setToken } = useToken();

  if (!token) {
    return <Login setToken={setToken} />;
  }

  return (
    <HashRouter>
      <Switch>
        <Route
          exact
          path="/login"
          name="Iniciar Sesión"
          render={(props) => <Login {...props} />}
        />
        <ProtectedRoute
          exact
          path="/register"
          name="registro"
          roles={[rol.admin, rol.plataforma]}
          component={(props) => <Register {...props} />}
        />
        <Route
          exact
          path="/404"
          name="Page 404"
          render={(props) => <Page404 {...props} />}
        />
        <Route
          exact
          path="/500"
          name="Page 500"
          render={(props) => <Page500 {...props} />}
        />
        <Route
          path="/"
          name="Home"
          render={(props) => <TheLayout {...props} />}
        />
      </Switch>
      {/* </React.Suspense> */}
    </HashRouter>
  );
}

export default App;
