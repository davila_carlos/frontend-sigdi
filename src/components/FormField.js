import { CFormGroup, CLabel, CInput, CInvalidFeedback } from "@coreui/react";
import React from "react";

export default function FormField(props) {
  return (
    <CFormGroup className="mb-3">
      <CLabel htmlFor={props.name}>{props.label}</CLabel>
      <CInput
        type={props.type}
        name={props.name}
        value={props.value}
        invalid={props.errors && true}
        innerRef={props.register}
      />
      <CInvalidFeedback>{props.errors?.message}</CInvalidFeedback>
    </CFormGroup>
  );
}
