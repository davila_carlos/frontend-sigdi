import {
  CFormGroup,
  CInputGroup,
  CInputGroupAppend,
  CLabel,
  CInput,
  CButton,
  CInvalidFeedback,
} from "@coreui/react";
import React, { useState } from "react";
import { BiShow, BiHide } from "react-icons/bi";

export default function PasswordField(props) {
  const [type, setType] = useState("password");

  return (
    <CFormGroup className="mb-3">
      <CLabel htmlFor={props.name}>{props.label}</CLabel>
      <CInputGroup>
        <CInput
          type={type}
          name={props.name}
          innerRef={props.register}
          invalid={props.errors && true}
        />

        <CInputGroupAppend>
          <CButton
            tabIndex="-1"
            color="light"
            onClick={() => setType(type === "password" ? "input" : "password")}
          >
            {type === "password" ? (
              <BiShow size="1.2em" />
            ) : (
              <BiHide size="1.2em" />
            )}
          </CButton>
        </CInputGroupAppend>
        <CInvalidFeedback>{props.errors?.message}</CInvalidFeedback>
      </CInputGroup>
    </CFormGroup>
  );
}
