import { useState } from "react";

export default function useToken() {
  const getToken = () => {
    const tokenString = sessionStorage.getItem("token");
    const userToken = JSON.parse(tokenString);
    return userToken;
  };

  const [token, setToken] = useState(getToken());

  const saveToken = (userToken) => {
    if (userToken.resultado) {
      sessionStorage.setItem("token", JSON.stringify(userToken));
      setToken(userToken);
    } else {
      console.log("no resultado");
    }
  };

  const hasAnyRole = (roles) => {
    return !roles || roles.includes(getToken()?.idrol);
  };

  return {
    token,
    setToken: saveToken,
    hasAnyRole,
  };
}
