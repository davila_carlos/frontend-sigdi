import React from "react";
import { CSidebarNavDropdown } from "@coreui/react";
import useToken from "./useToken";

export default function ProtectedSidebarNavDropdown({ roles, ...props }) {
  const { hasAnyRole } = useToken();

  return (
    <CSidebarNavDropdown
      style={{ display: hasAnyRole(roles) ? "" : "none" }}
      {...props}
    />
  );
}
