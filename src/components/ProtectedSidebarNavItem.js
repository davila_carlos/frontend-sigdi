import React from "react";
import { CSidebarNavItem } from "@coreui/react";
import useToken from "./useToken";

export default function ProtectedSidebarNavItem({ roles, ...props }) {
  const { hasAnyRole } = useToken();

  return (
    <CSidebarNavItem
      style={{ display: hasAnyRole(roles) ? "" : "none" }}
      {...props}
    />
  );
}
