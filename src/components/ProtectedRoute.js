import React from "react";
import { Route, Redirect } from "react-router-dom";
import useToken from "./useToken";

export default function ProtectedRoute({
  component: Component,
  roles,
  ...rest
}) {
  const { hasAnyRole } = useToken();

  return (
    <Route
      {...rest}
      render={(props) => {
        if (hasAnyRole(roles)) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/404",
                state: {
                  from: props.location,
                },
              }}
            />
          );
        }
      }}
    />
  );
}
