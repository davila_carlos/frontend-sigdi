import React from 'react';
import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    //CCollapse,
    //CDropdownItem,
    //CDropdownMenu,
    //CDropdownToggle,
    //CFade,
    CForm,
    CFormGroup,
    CFormText,
    //CValidFeedback,
    //CInvalidFeedback,
    //CTextarea,
    CInput,
    //CInputFile,
    //CInputCheckbox, 
    //CInputRadio,
    //CInputGroup,
    //CInputGroupAppend,
    //CInputGroupPrepend,
    //CDropdown,
    //CInputGroupText,
    CLabel,
    //CSelect,
    CRow
  } from '@coreui/react'
  import CIcon from '@coreui/icons-react'

class TipoParteForm extends React.Component{

    state={
        descripcion:'',
    };

    //const [collapsed, setCollapsed] = React.useState(true);
    //const [showElements, setShowElements] = React.useState(true);

    /*constructor(props){
        super(props);
        this.state={
            descripcion:'',
        };
        this.handleChangeDescripcion = this.handleChangeDescripcion.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }*/

  

    handleChange = e => {
        //console.log({
            //name: e.target.name,
            //value:e.target.value,
        //});
        this.setState({
            [e.target.name]: e.target.value,
        });
    };
    
    /*handleClick = e => {
        console.log("hola quetal");
    } */  
    
    /*handleSubmit (event) {
       
        console.log("hola submit");
        fetch('http://localhost:3000/api/tipoparte',{
            method:'POST',
            headers:{
                'Access-Control-Allow-Origin: *',
                //'Accept':'application/json',
                "Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept",
                'content-type: application/json; charset=utf-8',
                'Access-Control-Allow-Origin':'GET, POST,PUT,DELETE',
                
            },
            body:JSON.stringify({
                descripcion: this.state.descripcion,
            })

        }).then();

        event.preventDefault();
    }  */  

    handleSubmit = e => {

        e.preventDefault();
        console.log('Form was submitted');
        console.log(this.state);
    };


    render(){

        return(
            <CRow>        
                <CCol xs="12" md="12">
                   
                    <CCard>
                        <CCardHeader>
                            Formulario de Registro
                            <small> Tipo Parte</small>
                        </CCardHeader>
                        <CCardBody>
                            <CForm >
                                <CFormGroup>
                                    <CLabel htmlFor="nf-descripcion">Descripcion</CLabel>
                                    <CInput  
                                        type="input" 
                                        id="descripcion" 
                                        name="descripcion" 
                                        placeholder=""                                         
                                        onChange={this.handleChange} 
                                        value={this.state.descripcion}
                                    />
                                    <CFormText className="help-block">Introduzca la descripcion del tipo de parte</CFormText>
                                </CFormGroup>
                            </CForm>
                        </CCardBody>
                        <CCardFooter>
                            <CButton   onClick={this.handleSubmit} size="sm" color="info"><CIcon name="cil-save" /> Guardar</CButton> 
                        </CCardFooter>
                    </CCard>

                </CCol>
            </CRow>
        );

    }



   
}

export default TipoParteForm;