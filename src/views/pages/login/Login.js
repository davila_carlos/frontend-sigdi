import React from "react";
import PropTypes from "prop-types";
import FormField from "../../../components/FormField";
import PasswordField from "../../../components/PasswordField";
import CIcon from "@coreui/icons-react";
import { useForm } from "react-hook-form";
import { urlserviciosweb } from "../../config";
import {
  CContainer,
  CRow,
  CCol,
  CCard,
  CCardBody,
  CForm,
  CButton,
  CCardGroup,
  CAlert,
} from "@coreui/react";
import { useState } from "react/cjs/react.development";

async function loginUser(credentials) {
  return fetch(`${urlserviciosweb}/usuario/logueo`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  })
    .then((res) => {
      if (res.ok) return res.json();
    })
    .catch((err) => console.log(err));
}

export default function Login({ setToken }) {
  const { register, handleSubmit, errors } = useForm();
  const [message, setMessage] = useState("");
  const [visible, setVisible] = useState(5);

  async function onSubmit(credentials) {
    const token = await loginUser(credentials);
    console.log("tkn", token);
    if (token?.resultado) {
      setToken(token);
    } else if (!token?.resultado) {
      setMessage("Usuario o contraseña incorrectos.");
      setVisible(5);
    } else {
      setMessage("El servicio no está disponible.");
      setVisible(5);
    }
  }

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="9" lg="8" xl="7">
            <CCardGroup>
              <CCard>
                <CCardBody className="p-4">
                  <CForm onSubmit={handleSubmit(onSubmit)}>
                    <CRow className="justify-content-center mb-4">
                      <CIcon
                        className="c-sidebar-brand-full"
                        src="./logo_sigdi.B.fw.png"
                        height={70}
                      />
                    </CRow>

                    {message && (
                      <CAlert
                        color="warning"
                        show={visible}
                        onShowChange={setVisible}
                      >
                        {message}
                      </CAlert>
                    )}

                    <FormField
                      errors={errors.username}
                      name="username"
                      label="Usuario"
                      register={register({
                        required: {
                          value: true,
                          message: "Este campo es requerido",
                        },
                      })}
                    />
                    <PasswordField
                      name="userpass"
                      label="Contraseña"
                      errors={errors.userpass}
                      register={register({
                        required: {
                          value: true,
                          message: "Este campo es requerido",
                        },
                      })}
                    />
                    <CButton type="submit" color="info" block>
                      Ingresar
                    </CButton>
                  </CForm>
                </CCardBody>
              </CCard>

              <CCard
                className="text-white active py-4 d-md-down-none"
                style={{ width: "44%", backgroundColor: "#40a0ff" }}
              >
                <CCardBody className="text-center">
                  <div>
                    <h1>¡Bienvenido!</h1>
                    <br></br>
                    <h3>
                      Le damos la bienvenida al SIGDI <br />
                      <br />
                      <i>"Sistema de Gestión Disciplinaria"</i>
                    </h3>
                    <br></br>
                    <br />
                    <p className="text-right">
                      Consejo de la Magistratura
                      <br /> Órgano Judicial de Bolivia.
                    </p>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired,
};
