import React from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CRow,
} from "@coreui/react";
import PasswordField from "../../components/PasswordField";
import { useForm } from "react-hook-form";
import useToken from "../../components/useToken";
import { urlserviciosweb } from "../config";
import { useHistory } from "react-router-dom";

export default function CambioClaveForm() {
  const { handleSubmit, register, errors, watch } = useForm();
  const { token } = useToken();
  const history = useHistory();

  let watchNewuserpass = watch("newuserpass");

  async function onSubmit(data) {
    await fetch(`${urlserviciosweb}/usuario/cambiarclave`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ iduser: token.idusuario, ...data }),
    })
      .then((res) => {
        if (res.ok) {
          alert("Clave cambiada con éxito.");
          history.goBack();
        }
      })
      .catch((err) => console.log(err));
  }

  return (
    <CContainer>
      <CRow className="justify-content-center">
        <CCol md="9" lg="7" xl="6">
          <CCard>
            <CCardBody className="p-4">
              <h2>Cambio de clave</h2>

              <CForm onSubmit={handleSubmit(onSubmit)}>
                <PasswordField
                  label="Clave actual"
                  name="userpass"
                  errors={errors.userpass}
                  register={register({
                    required: {
                      value: true,
                      message: "Este campo es requerido",
                    },
                  })}
                />

                <PasswordField
                  label="Nueva clave"
                  name="newuserpass"
                  errors={errors.newuserpass}
                  register={register({
                    required: {
                      value: true,
                      message: "Este campo es requerido",
                    },
                  })}
                />

                <PasswordField
                  label="Confirme nueva clave"
                  name="nuevouserpass"
                  register={register({
                    required: {
                      value: true,
                      message: "Este campo es requerido",
                    },
                    validate: {
                      isEqual: (value) =>
                        value === watchNewuserpass || "Las claves no coinciden",
                    },
                  })}
                  errors={errors.nuevouserpass}
                />

                <CButton type="submit" color="success">
                  Cambiar clave
                </CButton>
              </CForm>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </CContainer>
  );
}
