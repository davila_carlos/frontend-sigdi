import React, { useEffect, useState } from "react";
import UsuarioCard from "./UsuarioCard";
import { urlserviciosweb } from "../config";
import {
  CButton,
  CCard,
  CCardBody,
  CInput,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CInputGroupText,
  CListGroup,
} from "@coreui/react";
import { BiSearch, BiX } from "react-icons/bi";

export default function Usuarios() {
  let [users, setUsers] = useState([]);
  let [query, setQuery] = useState("");
  let [usersFiltered, setUsersFiltered] = useState([]);

  useEffect(() => {
    fetch(`${urlserviciosweb}/usuario`)
      .then((response) => response.json())
      .then((res) => {
        setUsers(res.data);
        setUsersFiltered(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    setUsersFiltered(
      users.filter((u) => {
        const fullName = `${u.nombre} ${u.paterno} ${u.materno}`;
        return fullName.toLowerCase().includes(query.toLowerCase());
      })
    );
  }, [query, users]);

  function handleChange(e) {
    setQuery(e.target.value);
  }

  return (
    <CCard>
      <CCardBody>
        <h1>Lista de usuarios</h1>

        <CInputGroup>
          <CInputGroupPrepend>
            <CInputGroupText>
              <BiSearch />
            </CInputGroupText>
          </CInputGroupPrepend>

          <CInput placeholder="Buscar" value={query} onChange={handleChange} />

          {query && (
            <CInputGroupAppend>
              <CButton color="light" onClick={() => setQuery("")}>
                <BiX />
              </CButton>
            </CInputGroupAppend>
          )}
        </CInputGroup>
        <br />

        {!usersFiltered ? (
          <h3>No existen usuarios</h3>
        ) : (
          <CListGroup>
            {usersFiltered.map((u) => (
              <UsuarioCard
                key={u.idusuario}
                id={u.idusuario}
                name={`${u.nombre} ${u.paterno} ${u.materno}`}
                email={u.email}
              />
            ))}
          </CListGroup>
        )}
      </CCardBody>
    </CCard>
  );
}
