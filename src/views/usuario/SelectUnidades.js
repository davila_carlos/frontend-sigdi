import React, { useEffect, useState } from "react";
import { CFormGroup, CLabel, CSelect } from "@coreui/react";
import { urlserviciosweb } from "../config";

export default function SelectUnidades(props) {
  const [unidades, setUnidades] = useState([]);

  useEffect(() => {
    fetch(`${urlserviciosweb}/unidad`)
      .then((res) => {
        if (res.ok) {
          res.json().then(({ data }) => {
            setUnidades(
              data.filter(
                (u) =>
                  u.estaactivo &&
                  u.iddepartamento === parseInt(props.iddepartamento)
              )
            );
          });
        }
      })
      .catch((err) => console.log(err));
  }, [props.iddepartamento]);

  return (
    <CFormGroup>
      <CLabel htmlFor="unidad"> Unidad</CLabel>
      <CSelect
        custom
        innerRef={props.innerRef}
        name="idunidad"
        value={props.value}
      >
        {unidades.map(({ idunidad, descripcion }) => (
          <option key={idunidad} value={idunidad}>
            {descripcion}
          </option>
        ))}
      </CSelect>
    </CFormGroup>
  );
}
