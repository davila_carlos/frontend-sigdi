import React from "react";
import {
  CButton,
  CCardSubtitle,
  CCardTitle,
  CCol,
  CListGroupItem,
  CRow,
} from "@coreui/react";
import { BiEdit, BiReset, BiTrash } from "react-icons/bi";
import { urlserviciosweb } from "../config";
import { useHistory } from "react-router";

function UsuarioCard(props) {
  const history = useHistory();

  function eliminarUsuario(id) {
    fetch(`${urlserviciosweb}/usuario/${id}`, {
      method: "DELETE",
    })
      .then((res) => {
        if (res.ok) {
          alert("Usuario eliminado.");
          history.go(0);
        }
      })
      .catch((err) => console.log(err));
  }

  function resetearClave() {
    const req = JSON.stringify({ iduser: parseInt(props.id) });
    console.log(req);

    fetch(`${urlserviciosweb}/usuario/resetearclave`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: req,
    })
      .then((res) => {
        if (res.ok) {
          alert("Clave reinciada.");
          history.go(0);
        }
      })
      .catch((err) => console.log(err));
  }

  return (
    <CListGroupItem className="py-3">
      <CRow>
        <CCol sm="6">
          <CCardTitle className="center">{props.name}</CCardTitle>
          <CCardSubtitle>{props.email}</CCardSubtitle>
        </CCol>

        <CButton
          size="sm"
          color="dark"
          className="ml-auto"
          onClick={() => history.push(`usuarios/editar/${props.id}`)}
        >
          <BiEdit /> Editar
        </CButton>

        <CButton
          size="sm"
          color="danger"
          className="mx-1"
          onClick={() => eliminarUsuario(props.id)}
        >
          <BiTrash /> Eliminar
        </CButton>

        <CButton
          size="sm"
          color="warning"
          className="mx-1"
          onClick={() => resetearClave()}
        >
          <BiReset /> Resetear clave
        </CButton>
      </CRow>
    </CListGroupItem>
  );
}
export default UsuarioCard;
