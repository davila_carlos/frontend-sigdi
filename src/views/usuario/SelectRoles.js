import { CFormGroup, CLabel, CSelect } from "@coreui/react";
import React, { useEffect, useState } from "react";
import { urlserviciosweb } from "../config";

export default function SelectRoles(props) {
  const [roles, setRoles] = useState([]);

  useEffect(() => {
    fetch(`${urlserviciosweb}/rol`)
      .then((res) => {
        if (res.ok) {
          res.json().then((roles) => setRoles(roles.data));
        }
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <CFormGroup>
      <CLabel htmlFor="rol">Rol</CLabel>
      <CSelect custom innerRef={props.innerRef} name="idrol">
        {roles
          .filter((rol) => rol.estaactivo)
          .map(({ idrol, descripcion }) => (
            <option
              key={idrol}
              value={idrol}
              disabled={descripcion === "Administrador"}
            >
              {descripcion}
            </option>
          ))}
      </CSelect>
    </CFormGroup>
  );
}
