import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CLabel,
  CRow,
  CSelect,
} from "@coreui/react";
import SelectRoles from "./SelectRoles";
import SelectUnidades from "./SelectUnidades";
import FormField from "../../components/FormField";

import { useForm } from "react-hook-form";
import { urlserviciosweb } from "../config";
import { useHistory } from "react-router-dom";

function Form(props) {
  const { handleSubmit, register, errors, reset } = useForm();
  const [iddepartamento, setIddepartamento] = useState(0);
  const [idUnidad, setIdUnidad] = useState();
  const [user, setUser] = useState();

  const history = useHistory();

  useEffect(() => {
    fetch(`${urlserviciosweb}/usuario/${props.match.params.id}`)
      .then((res) => res.json())
      .then((user) => {
        setUser(user);
        setIdUnidad(user.idunidad);

        fetch(`${urlserviciosweb}/unidad/${user.idunidad}`)
          .then((res) => res.json())
          .then((res) => setIddepartamento(res.iddepartamento));
      })
      .catch((err) => console.log(err));
  }, [props.match.params.id]);

  useEffect(() => {
    reset({
      ...user,
      fechanacimiento: user?.fechanacimiento.replace(/T.*/, ""),
    });
  }, [user, reset]);

  function handleChange(event) {
    setIddepartamento(event.target.value);
  }

  async function onSubmit(data) {
    await fetch(`${urlserviciosweb}/usuario/${props.match.params.id}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })
      .then((res) => {
        if (res.ok) {
          alert("Usuario editado");
          history.push("/usuarios");
        }
      })
      .catch((res) => alert(JSON.stringify(res)));
  }

  return (
    <CContainer>
      <CRow className="justify-content-center">
        <CCol md="10" lg="8" xl="7">
          <CCard className="mx-4">
            <CCardBody className="p-4">
              <CForm onSubmit={handleSubmit(onSubmit)}>
                <h1>Editar usuario</h1>

                <SelectRoles innerRef={register} />

                <CRow>
                  <CCol sm="5" className="mb-3">
                    <CLabel htmlFor="distrito">Distrito </CLabel>
                    <CSelect
                      custom
                      name="distrito"
                      onChange={handleChange}
                      value={iddepartamento}
                    >
                      <option value="0">Sin distrito</option>
                      <option value="1">Chuquisaca</option>
                      <option value="2">La Paz</option>
                      <option value="3">Cochabamba</option>
                      <option value="4">Santa Cruz</option>
                      <option value="5">Tarija</option>
                      <option value="6">Oruro</option>
                      <option value="7">Potosí</option>
                      <option value="8">Pando</option>
                      <option value="9">Beni</option>
                    </CSelect>
                  </CCol>

                  <CCol sm="7">
                    <SelectUnidades
                      innerRef={register}
                      iddepartamento={iddepartamento}
                      value={idUnidad}
                    />
                  </CCol>
                </CRow>

                <FormField
                  name="nombre"
                  label="Nombre(s)"
                  errors={errors.nombre}
                  register={register({
                    required: {
                      value: true,
                      message: "El nombre es requerido",
                    },
                  })}
                />
                <CRow>
                  <CCol sm="6">
                    <FormField
                      name="paterno"
                      label="Apellido paterno"
                      register={register}
                    />
                  </CCol>

                  <CCol sm="6">
                    <FormField
                      name="materno"
                      label="Apellido materno"
                      register={register}
                    />
                  </CCol>
                </CRow>
                <FormField
                  type="date"
                  name="fechanacimiento"
                  label="Fecha de nacimiento"
                  register={register}
                />
                <CRow>
                  <CCol sm="4">
                    <FormField
                      name="ci"
                      label="Carnet de Identidad"
                      errors={errors.ci}
                      register={register({
                        required: {
                          value: true,
                          message: "El carnet es requerido",
                        },
                        pattern: {
                          value: /\b[0-9]{5,9}\b/,
                          message: "El formato no es correcto",
                        },
                      })}
                    />
                  </CCol>

                  <CCol sm="4">
                    <FormField
                      name="complemento"
                      label="Complemento"
                      errors={errors.complemento}
                      register={register({
                        maxLength: {
                          value: 2,
                          message: "Longitud excedida",
                        },
                      })}
                    />
                  </CCol>

                  <CCol sm="4" className="mb-3">
                    <CLabel htmlFor="idexpedido">Expedido</CLabel>
                    <CSelect custom name="idexpedido" innerRef={register}>
                      <option value="10">No sabe</option>
                      <option value="1">Chuquisaca</option>
                      <option value="2">La Paz</option>
                      <option value="3">Cochabamba</option>
                      <option value="4">Santa Cruz</option>
                      <option value="5">Tarija</option>
                      <option value="6">Oruro</option>
                      <option value="7">Potosí</option>
                      <option value="8">Pando</option>
                      <option value="9">Beni</option>
                    </CSelect>
                  </CCol>
                </CRow>
                <FormField
                  name="email"
                  type="email"
                  label="Correo electrónico"
                  errors={errors.email}
                  register={register({
                    required: {
                      value: true,
                      message: "El correo electrónico es requerido",
                    },
                    pattern: {
                      value:
                        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                      message: "El formato no es válido",
                    },
                  })}
                />
                <FormField
                  name="direccion"
                  label="Dirección"
                  errors={errors.direccion}
                  register={register({
                    required: {
                      value: true,
                      message: "La dirección es requerida",
                    },
                  })}
                />
                <FormField
                  name="telefono"
                  label="Celular"
                  errors={errors.telefono}
                  register={register({
                    required: {
                      value: true,
                      message: "El celular es requerido",
                    },
                    pattern: {
                      value: /[0-9]{8}/,
                      message: "El formato no es válido",
                    },
                    maxLength: {
                      value: 8,
                      message: "Longitud excedida",
                    },
                  })}
                />
                <FormField
                  name="telefonooficina"
                  label="Teléfono oficina"
                  errors={errors.telefonooficina}
                  register={register}
                />
                <CButton type="submit" color="success" block>
                  Registrar
                </CButton>
              </CForm>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </CContainer>
  );
}

export default Form;
