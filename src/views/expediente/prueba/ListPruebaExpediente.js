import React from "react";
import {
  CCol,
  CCard,
  CCardBody,
  CDataTable,
  CRow,
  CCardHeader,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import fileDownload from "js-file-download";

import { urlserviciosweb } from "../conffile/config";

const ListPruebaExpediente = (props) => {
  //-------------Obtener Partes de Expediente-------------------------------------
  const [listaPruebasExpediente, SetListaPruebasExpediente] = React.useState(
    []
  );

  React.useEffect(() => {
    async function obtenerpruebasexpediente() {
      const data = await fetch(
        `${urlserviciosweb}prueba/pruebasexpediente/${props.idexpediente}`
      );
      if (data.status === 200) {
        const listpruebasexp = await data.json();
        SetListaPruebasExpediente(listpruebasexp.data);
      }
    }

    //obtenerpruebasexpediente();       //---En ExpedienteDetalle.js habilitar: window.location.reload(true)

    const timer = setInterval(() => {
      obtenerpruebasexpediente();
    }, 1000);
    return () => clearInterval(timer);
  }, [props.idexpediente]);

  const seleccionarArchivo = (archivo) => {
    //console.log("mostrando nombre archivo");
    //console.log(archivo);

    async function obtenerArchivoPrueba() {
      const respuesta = await fetch(
        `${urlserviciosweb}prueba/archivoprueba/${archivo}`
      );
      const blob = await respuesta.blob();
      //console.log(blob);
      fileDownload(blob, archivo);
    }
    obtenerArchivoPrueba();
  };

  return (
    <>
      <CRow>
        <CCol xs="12" md="12">
          <CCard borderColor="success">
            <CCardHeader>
              <strong>PRUEBAS DE EXPEDIENTE :</strong>
            </CCardHeader>

            <CCardBody>
              {listaPruebasExpediente.length > 0 ? (
                <CDataTable
                  items={listaPruebasExpediente}
                  fields={[
                    {
                      key: "idtipoprueba",
                      label: "Tipo de Prueba",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                    {
                      key: "numerofojas",
                      label: "Número de Hojas",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                    {
                      key: "descripcion",
                      label: "Número de Hojas",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                    {
                      key: "archivoprueba",
                      label: "Archivo de Prueba Adjunto",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                  ]}
                  //tableFilter={{'placeholder':'Palabra a Buscar...........', 'label':'BUSCAR DENUNCIA :'}}
                  //dark
                  hover
                  sorter
                  striped
                  bordered
                  size="sm"
                  itemsPerPage={10}
                  //pagination
                  scopedSlots={{
                    idtipoprueba: (item) => (
                      <td>{item.tipoprueba.descripcion}</td>
                    ),
                    numerofojas: (item) => <td>{item.numerofojas}</td>,
                    descripcion: (item) => <td>{item.descripcion}</td>,
                    archivoprueba: (item) => (
                      <td className="py-2">
                        <CButton
                          size="sm"
                          color="danger"
                          title={`Archivo de Prueba - ${item.ubicacionfisicadocumento}`}
                          onClick={() =>
                            seleccionarArchivo(item.ubicacionfisicadocumento)
                          }
                        >
                          <CIcon name="cil-cloud-download" />
                        </CButton>
                      </td>
                    ),
                  }}
                />
              ) : (
                <div></div>
              )}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default ListPruebaExpediente;
