import React from 'react';
import {

    CCol, 
    CCardBody,
    CButton,
    CLabel,
    CFormText,
    CCardFooter,
    CFormGroup,
    
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
//import ChartLineSimple from '../charts/ChartLineSimple';
import { urlserviciosweb } from '../conffile/config';
import { useForm } from 'react-hook-form';

const PruebaForm = (props) => {


    //-------------------Datos Expediente-----------------------------
    const [cudeexpediente,SetCudeExpediente ] = React.useState(0);
    
    React.useEffect(()=>{
        async function obtenerdatosexpediente(){
            const data = await fetch(`${urlserviciosweb}expediente/${props.idexpediente}`);
            if(data.status===200)
            {
                const datos = await data.json(); 
                SetCudeExpediente(datos.data.cude); 
            }
        }
        obtenerdatosexpediente();       
    },[props.idexpediente]); 


    //-------------------Lista de Tipo Prueba-----------------------------    
    const [listatipoprueba, SetListaTipoPrueba] = React.useState([]);   
    React.useEffect(()=>{

        async function obtenertipoprueba(){

            const data = await fetch(`${urlserviciosweb}tipoprueba`);
            if(data.status===200)
            {
                const list = await data.json();               
                SetListaTipoPrueba(list.data); 
            }
        }
        obtenertipoprueba();       
    },[]);


    //----------------Obtener Valores de Formulario Registro de Parte--------------

    const {register, errors, handleSubmit} = useForm();
    const [entradasprueba, setentradasPrueba] = React.useState([]);

    const procesarFormularioPrueba = (datos, e) => {
        //console.log(datos);
        setentradasPrueba([
          ...entradasprueba,
          datos
        ])
        // limpiar campos        
        registrarPrueba();
        e.target.reset();     
    }; 

    async function registrarPrueba(){

        //evento.preventDefault();        
        
        let myForm = document.getElementById('formularioprueba');   
        let formData = new FormData(myForm);
                
        //console.log(formData);
                    
        const respuesta = await fetch(`${urlserviciosweb}prueba `,{
            method:'POST',
            body:formData,
            headers:{ },           
        });
        const mensaje = await respuesta.json();
        alert(mensaje.message);
            
        //document.getElementById("formularioprueba").reset();
    };     

    
    return (
    
    <>
        <form  className="form-horizontal" id="formularioprueba" onSubmit={handleSubmit(procesarFormularioPrueba)} >
                            
            <CCardBody>

                <CFormGroup row> 
                    <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>CUDE  Expediente:<code>(*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">   
                        <input 
                            type="hidden"
                            id="idexpediente" 
                            name="idexpediente" 
                            value={props.idexpediente} 
                            ref={register({})}
                        />                                                                                              
                        <strong>{cudeexpediente}</strong>
                    </CCol> 
                </CFormGroup> 

                <CFormGroup row> 
                    <CCol xs="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>Seleccionar el Tipo de Prueba: <code>(*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">   
                        <select 
                            //custom 
                            name="idtipoprueba" 
                            id="idtipoprueba"  
                            className="form-control my-2"
                            ref={register({})}
                        >
                            <option value="0">Seleccionar tipo Prueba.....</option>
                                {listatipoprueba.length>0?listatipoprueba.map((element) => (        
                                <option key={ element.idtipoprueba} value={ element.idtipoprueba}>{ element.descripcion}</option>
                                )):("Sin Elementos")} 
                        </select>                                     
                        <CFormText>Ejem: Grabación Multimedia, Documento, etc</CFormText>
                                        
                    </CCol>                                   
                </CFormGroup> 

                <CFormGroup row> 
                    <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>Numero de fojas<code>(*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">                    
                        <input
                            type="text" 
                            id="numerofojas" 
                            name="numerofojas" 
                            placeholder="Número de fojas......"  
                            className="form-control my-2"
                            ref={
                                register({
                                    required: { value:true, message: 'Debe Introducir el número de fojas.' },
                                    pattern:  { value:  /^[0-9]{1,8}$/i, message: 'Introduzca números enteros, maximo de 3 dígitos'},
                                                
                                })
                            }
                        />
                        <CFormText>Ejem: 1,2,3</CFormText>
                        <span className="text-danger text-small d-block mb-2">
                            {errors?.numerofojas?.message}
                        </span>
                    </CCol> 
                </CFormGroup> 
                                
                <CFormGroup row> 
                    <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>Adjuntar el Archivo de la Prueba Presentada (pdf, doc, docx) :<code>(*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">                    
                        <input 
                            type="file" 
                            id="ubicacionfisicadocumento" 
                            name="ubicacionfisicadocumento" 
                            className="form-control my-2"
                            ref={
                                register({
                                    required: { value:true, message: 'Debe Adjuntar el Archivo de la Prueba Presentada (pdf,doc,docx,mp4,mp3)' }
                                })
                            }                    
                        />
                        <CFormText>Seleccione el Archivo de la Prueba Presentada: </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                            {errors?.ubicacionfisicadocumento?.message}
                        </span>
                    </CCol> 
                </CFormGroup> 

                <CFormGroup row> 
                    <CCol xs="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>La prueba presentada se: <code>(*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">   
                        <input 
                            type="radio" 
                            name="aceptada"  
                            value = "true"
                            ref={register({})}                          
                        /> Acepta {       }                                
                        <input 
                            type="radio" 
                            name="aceptada"
                            value="false"  
                            ref={register({})}                           
                        /> Rechaza                                        
                    </CCol>                                   
                </CFormGroup>    
                <CFormGroup row> 
                    <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>Descripcion de la Prueba:<code>(*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">  
                        <textarea
                            //className="form-control"
                            name="descripcion" 
                            id="descripcion"  
                            className="form-control my-2"                                                
                            rows="5"
                            ref={
                                register({                                                        
                                    pattern:  { value: /^[.,;:0-9A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i, message: 'Se permite introducir letras, números, signos de puntuación " . "  ,  " , " , " : " , " ; " '}
                                })
                            } 
                        />
                        <CFormText>Introduzca la descripcion de la prueba.</CFormText>
                            <span className="text-danger text-small d-block mb-2">
                                {errors?.descripcion?.message}
                            </span>
                    </CCol> 
                </CFormGroup>                             

                <CCardFooter>
                    <CButton type="submit"  size="sm" color="danger" ><CIcon name="cil-save" /> Guardar</CButton>   
                </CCardFooter>
                                                            
            </CCardBody>

        </form>

    </>

    )

}

    
export default PruebaForm;