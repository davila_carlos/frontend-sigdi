import React from 'react';
import { useParams } from "react-router";
import ExpedienteReporte from './ExpedienteReporte';
//import ReactPDF from '@react-pdf/renderer';
//import ReactDOM from 'react-dom';
import { PDFViewer } from '@react-pdf/renderer';
import { PDFDownloadLink} from '@react-pdf/renderer';
import { Document, Page, Text, View, StyleSheet } from '@react-pdf/renderer';
import ReactPDF from '@react-pdf/renderer';
import { urlserviciosweb } from './conffile/config';


 
//ReactDOM.render(<App />, document.getElementById('root'));*/

const Expediente = () => {


    const { idexpediente } = useParams();

    console.log(idexpediente);

      
}


const styles = StyleSheet.create({
    page: {
    flexDirection: 'row',
    backgroundColor: '#E4E4E4'
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1


        /*width: 200,
        '@media max-width: 400': {
          width: 300,
        },
        '@media orientation: landscape': {
          width: 400,
        },   */         
    }
});




const MyDocument = (props) => (

    <Document>
        <Page size="A4" style={styles.page}>
            <View style={styles.section}>
            {props.listapartesexpediente.length>0?props.listapartesexpediente.map((element) =>( 
                
                <Text>
                    IdParte : {element.idparte}{"\n"} 
                    Nombre Completo : {element.nombrecompleto}{"\n"} 
                    Cedula de Identidad: {element.ci}{"\n"}  
                    Cargo Parte: {element.cargo}{"\n"} 
                    Unidad Parte : {element.unidad}{"\n"} 
                    Tipo Parte: {element.tipoparte}{"\n"}                     
                    {element.detallefalta.length>0?element.detallefalta.map((valores)=>(
                        <Text>
                            IdParte1 : {valores.idparte}{"\n"} 
                        </Text>
                    )):(<Text>..</Text>)}

                </Text>                 
            )):(<Text>..</Text>)} 
            </View>
        </Page>
    </Document>
);





const ExpedienteCaratula = () => {

    const { idexpediente } = useParams();

    console.log(idexpediente);


        
    //1.-------------------partesdeexpediente-------------------------------------
    const [listapartesexpediente, SetListaPartesExpediente] = React.useState([]);


        React.useEffect(()=>{

            const obtlistafaltasparte = async function (idparte){
                var respuesta = await fetch(`${urlserviciosweb}faltaparte/${idparte}`);  
                var response = await respuesta.json();
                return response;       
            }            
            
            async function obtenerpartesexpediente(){   

                const data = await fetch(`${urlserviciosweb}parte/partesexpediente/${idexpediente}`);            
                const list = await data.json();
                //console.log(list);        
                var hintArray = [];                
                list.data.map((a) =>{ 

                    hintArray.push({ 
                        nombrecompleto:`${a.persona.nombre} ${a.persona.paterno} ${a.persona.materno}`, 
                        ci:`${a.persona.ci}`,
                        cargo:`${a.cargo.descripcion}`, 
                        unidad:`${a.unidad.nombreunidadsinaes}`,
                        tipoparte:`${a.tipoparte.descripcion}`,
                        idparte:`${a.idparte}`,
                        detallefalta:[],
                    })

                    //console.log("array-original");
                    //console.log(hintArray);

                    hintArray.map((val) =>{ 
                        obtlistafaltasparte(val.idparte).then(item => {
                            item.data.map((d) =>{ 
                                val.detallefalta.push({
                                    idparte:`${d.idparte}`,
                                    falta:`${d.faltum.descripcion}`,
                                    descripcionfalta:`${d.descripciondenuncia}`,
                                });                          
                            })
                        });                         
                    });

                    //console.log("array-modificado");
                    //console.log(hintArray);                                   

                })                
                //console.log(hintArray);
                SetListaPartesExpediente(hintArray);                 
            }
            obtenerpartesexpediente();       
        },[]);        
    
    
        console.log("Lista partes expediente");
        console.log(listapartesexpediente);


      

    //ReactPDF.render(<ExpedienteReporte />, `${__dirname}/example.pdf`);

    //ReactPDF.render(<MyDocument />);

    return (
        <>    
    
            <h1>Hola quetal</h1>
            <PDFViewer>
                <MyDocument listapartesexpediente={listapartesexpediente}/>
            </PDFViewer>
            
        </>
    )    

}

export default ExpedienteCaratula
