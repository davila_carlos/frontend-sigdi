import React from "react"

const ExpedienteCard = ({ cude, relatohechos }) => ( //
    <article className="card">
        <div className="card__data s-border s-radius-br s-radius-bl s-pxy-2">
            <h3 className="center">{ cude }</h3>
            <div className="s-main-center">{ relatohechos }</div>
        </div>
    </article>
)

export default ExpedienteCard