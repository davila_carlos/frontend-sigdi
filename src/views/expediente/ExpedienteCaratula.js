import React from "react";
import { useParams } from "react-router";
// import ExpedienteReporte from "./ExpedienteReporte";
//import ReactPDF from '@react-pdf/renderer';
//import ReactDOM from 'react-dom';
import { PDFViewer } from "@react-pdf/renderer";
import { Document, Page, Text, View, StyleSheet } from "@react-pdf/renderer";
import { urlserviciosweb } from "./conffile/config";

//ReactDOM.render(<App />, document.getElementById('root'));*/

// const Expediente = () => {
//   const { idexpediente } = useParams();
//   console.log(idexpediente);
// };

const styles = StyleSheet.create({
  page: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
  },

  section: {
    margin: 20,
    padding: 20,
    flexGrow: 10,

    width: 200,
    "@media max-width: 400": {
      width: 300,
    },
    "@media orientation: landscape": {
      width: 400,
    },
  },
});

const MyDocument = (props) => (
  <Document>
    <Page size="A4" style={styles.page}>
      <View style={styles.section}>
        <Text>
          1.- PARTES DEL EXPEDIENTE{"\n"}
          {"\n"}
        </Text>
        {props.listapartesexpediente.length > 0 ? (
          props.listapartesexpediente.map((element) => (
            <Text>
              IdParte : {element.idparte}
              {"\n"}
              Nombre Completo : {element.nombrecompleto}
              {"\n"}
              Cedula de Identidad: {element.ci}
              {"\n"}
              Cargo Parte: {element.cargo}
              {"\n"}
              Unidad Parte : {element.unidad}
              {"\n"}
              Tipo Parte: {element.tipoparte}
              {"\n"}
              {"\n"}
            </Text>
          ))
        ) : (
          <Text>..</Text>
        )}

        <Text>
          2.- RELATO DE LOS HECHOS{"\n"}
          {"\n"}
        </Text>
        {props.listapartesexpediente.length > 0 ? (
          props.listapartesexpediente.map((element) =>
            props.listafaltaexpediente.length > 0 ? (
              props.listafaltaexpediente.map((item) =>
                element.idparte === item.idparte ? (
                  <Text>
                    Falta: {item.faltum.descripcion}
                    {"\n"}
                    Descripcion: {item.descripciondenuncia}
                    {"\n"} {"\n"}
                  </Text>
                ) : (
                  <Text></Text>
                )
              )
            ) : (
              <Text>..</Text>
            )
          )
        ) : (
          <Text>..</Text>
        )}

        <Text>
          3.- TIPO DE FALTAS{"\n"}
          {"\n"}
        </Text>
        {props.listafaltaexpediente.length > 0 ? (
          props.listafaltaexpediente.map((item) =>
            item.faltum.idtipofalta === 1 ? (
              <Text>
                Tipo Falta : Grave {"\n"}
                Falta : Tipo Parte: {item.faltum.descripcion}
                {"\n"}
                {"\n"}
              </Text>
            ) : item.faltum.idtipofalta === 2 ? (
              <Text>
                Tipo Falta : Leve {"\n"}
                Falta : Tipo Parte: {item.faltum.descripcion}
                {"\n"}
                {"\n"}
              </Text>
            ) : item.faltum.idtipofalta === 3 ? (
              <Text>
                Tipo Falta : Gravisima {"\n"}
                Falta : Tipo Parte: {item.faltum.descripcion}
                {"\n"}
                {"\n"}
              </Text>
            ) : (
              <Text>..</Text>
            )
          )
        ) : (
          <Text>..</Text>
        )}

        <Text>
          4.- PRUEBA DOCUMENTAL O INDICAR EL LUGAR DONDE PUDIERA SER HABIDA
          {"\n"}
          {"\n"}
        </Text>
        {props.listapruebasexpediente.length > 0 ? (
          props.listapruebasexpediente.map((item) => (
            <Text>
              Tipo de Prueba: {item.tipoprueba.descripcion} {"\n"}
              {"\n"}
            </Text>
          ))
        ) : (
          <Text>..</Text>
        )}

        <Text>
          5.- MEDIDA PRECAUTORIA - SOLICITADA {"\n"}
          {"\n"}
        </Text>

        <Text>
          6.- LUGAR Y FECHA DE DENUNCIA {"\n"}
          {"\n"}
        </Text>
        {props.datosexpediente.data ? (
          <Text>
            Sucre :{" "}
            {props.datosexpediente.data.fechahoraingresodenuncia.split("T")[0]}
            {"\n"}
            {"\n"}
          </Text>
        ) : (
          <Text>Sucre .......</Text>
        )}
      </View>
    </Page>
  </Document>
);

const ExpedienteCaratula = () => {
  const { idexpediente } = useParams();

  //1.-------------------partesdeexpediente-------------------------------------
  const [listapartesexpediente, SetListaPartesExpediente] = React.useState([]);
  const [listafaltaexpediente, SetListaFaltaExpediente] = React.useState([]);

  React.useEffect(() => {
    const obtlistafaltasparte = async function (idparte) {
      const respuesta = await fetch(`${urlserviciosweb}faltaparte/${idparte}`);
      if (respuesta.status === 200) {
        const response = await respuesta.json();
        return response;
      }
    };

    async function obtenerpartesexpediente() {
      const data = await fetch(
        `${urlserviciosweb}parte/partesexpediente/${idexpediente}`
      );
      if (data.status === 200) {
        const list = await data.json();
        //console.log(list);
        var hintArray = [];

        //if(list.data)
        //{

        for (let a of list.data) {
          hintArray.push({
            nombrecompleto: `${a.persona.nombre} ${a.persona.paterno} ${a.persona.materno}`,
            ci: `${a.persona.ci}`,
            cargo: `${a.cargo.descripcion}`,
            unidad: `${a.unidad.nombreunidadsinaes}`,
            tipoparte: `${a.tipoparte.descripcion}`,
            idparte: `${a.idparte}`,
          });

          //console.log("datosparte");
          //console.log(hintArray);

          /*obtlistafaltasparte(a.idparte).then(item => {
                            item.data.map((d) =>{

                                faltasArray.push({
                                    idparte:`${d.idparte}`,
                                    falta:`${d.faltum.descripcion}`,
                                    descripcionfalta:`${d.descripciondenuncia}`,
                                });


                            })
                        });  */
        };

        // var faltasArray = [];
        for(let b of hintArray) {
          //console.log(b.idparte);

          /*obtlistafaltasparte(b.idparte).then(item => {
                            //console.log("valores array");
                            const longitud = item.data;
                            console.log(longitud.length)

                            if(longitud.length>0)
                            {

                                longitud.map((d) =>{

                                    console.log(d.idparte);

                                    faltasArray.push({
                                        idparte:`${d.idparte}`,
                                        falta:`${d.faltum.descripcion}`,
                                        descripcionfalta:`${d.descripciondenuncia}`,
                                    });
                                })
                            }
                            else
                            {
                                console.log(b.idparte);
                            }
                        });   */

          obtlistafaltasparte(b.idparte).then((item) => {
            console.log("DATA");
            console.log(item.data);
            if (item.data) {
              const longitud = item.data;
              //console.log(longitud.length);
              SetListaFaltaExpediente(longitud);
            }
          });
        };
        SetListaPartesExpediente(hintArray);
        //}
        //else
        //{
        //console.log("No se encontraron partes de expediente");
        //}
      }
    }
    obtenerpartesexpediente();
  }, [idexpediente]);

  const [listapruebasexpediente, SetListaPruebasExpediente] = React.useState(
    []
  );

  React.useEffect(() => {
    async function obtenerpruebasexpediente() {
      const datos = await fetch(
        `${urlserviciosweb}prueba/pruebasexpediente/${idexpediente}`
      );
      if (datos.status === 200) {
        const listapruebas = await datos.json();
        //console.log(listapruebas);
        SetListaPruebasExpediente(listapruebas.data);
      }
    }
    obtenerpruebasexpediente();
  }, [idexpediente]);

  const [datosexpediente, SetDatosExpediente] = React.useState([]);

  React.useEffect(() => {
    async function obtenerdatosexpediente() {
      const datos = await fetch(`${urlserviciosweb}expediente/${idexpediente}`);
      const exp = await datos.json();
      console.log(exp);
      SetDatosExpediente(exp);
    }
    obtenerdatosexpediente();
  }, [idexpediente]);

  console.log("Lista partes expediente");
  console.log(listapartesexpediente);
  console.log("Lista faltas de partes");
  console.log(listafaltaexpediente);
  console.log("Lista pruebas de expediente");
  console.log(listapruebasexpediente);
  console.log("datos expediente");
  console.log(datosexpediente);

  //ReactPDF.render(<ExpedienteReporte />, `${__dirname}/example.pdf`);

  //ReactPDF.render(<MyDocument />);

  return (
    <>
      <PDFViewer width="1000" height="800">
        <MyDocument
          listapartesexpediente={listapartesexpediente}
          listafaltaexpediente={listafaltaexpediente}
          listapruebasexpediente={listapruebasexpediente}
          datosexpediente={datosexpediente}
        />
      </PDFViewer>
    </>
  );
};

export default ExpedienteCaratula;
