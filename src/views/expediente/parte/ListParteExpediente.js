import React from "react";
import {
  CCol,
  CCard,
  CCardBody,
  CDataTable,
  CRow,
  CCardHeader,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { urlserviciosweb } from "../conffile/config";

const ListParteExpediente = (props) => {
  //-------------Obtener Partes de Expediente-------------------------------------
  const [listaParteExpediente, SetListaParteExpediente] = React.useState([]);
  React.useEffect(() => {
    async function obtenerparteexpediente() {
      const data = await fetch(
        `${urlserviciosweb}parte/partesexpediente/${props.idexpediente}`
      );
      if (data.status === 200) {
        const listpartesexp = await data.json();
        SetListaParteExpediente(listpartesexp.data);
        //console.log(Object.keys(listpartesexp.data).length);
      }
    }

    //obtenerparteexpediente();       //---En ExpedienteDetalle.js habilitar: window.location.reload(true)

    const timer = setInterval(() => {
      obtenerparteexpediente();
    }, 1000);
    return () => clearInterval(timer);
  }, [props.idexpediente]);

  return (
    <>
      <CRow>
        <CCol xs="12" md="12">
          <CCard borderColor="warning">
            <CCardHeader>
              <strong>PARTES DE EXPEDIENTE :</strong>
            </CCardHeader>

            <CCardBody>
              {listaParteExpediente.length > 0 ? (
                <CDataTable
                  items={listaParteExpediente}
                  fields={[
                    {
                      key: "persona",
                      label: "Nombre Completo",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                    {
                      key: "cedulaidentidad",
                      label: "Cédula de Identidad",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                    {
                      key: "tipoparte",
                      label: "Tipo Parte",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                    {
                      key: "unidad",
                      label: "Unidad",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                    {
                      key: "cargo",
                      label: "Cargo",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                    {
                      key: "faltaparte",
                      label: "Falta Parte",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                    {
                      key: "direccionparte",
                      label: "Direccion Parte",
                      _style: { width: "20%" },
                      sorter: false,
                      filter: false,
                    },
                  ]}
                  //tableFilter={{'placeholder':'Palabra a Buscar...........', 'label':'BUSCAR DENUNCIA :'}}
                  //dark
                  hover
                  sorter
                  striped
                  bordered
                  size="sm"
                  itemsPerPage={10}
                  //pagination
                  scopedSlots={{
                    persona: (item) => (
                      <td>
                        {item.persona.nombre} {item.persona.paterno}{" "}
                        {item.persona.materno}
                      </td>
                    ),
                    cedulaidentidad: (item) => (
                      <td>
                        {item.persona.ci} - {item.persona.complemento}
                      </td>
                    ),
                    tipoparte: (item) => <td>{item.tipoparte.descripcion}</td>,
                    unidad: (item) => <td>{item.unidad.descripcion}</td>,
                    cargo: (item) => <td>{item.cargo.descripcion}</td>,
                    faltaparte: (item, index) => {
                      return (
                        <td className="py-2">
                          <CButton
                            size="sm"
                            color="danger"
                            title="Ver/Registrar Faltas de la Parte"
                            to={`/expediente/falta/form/${item.idparte}`}
                            disabled={item.idtipoparte !== 1}
                            //onClick={()=>abrirModalRegistrarFaltaParte(item.idparte)}
                          >
                            <CIcon name="cil-file" />
                          </CButton>
                        </td>
                      );
                    },
                    direccionparte: (item, index) => {
                      return (
                        <td className="py-2">
                          <CButton
                            size="sm"
                            color="success"
                            title="Ver/Registrar direccion de la Parte"
                            to={`/expediente/persona/direcciones/${item.idparte}`}
                          >
                            <CIcon name="cil-file" />
                          </CButton>
                        </td>
                      );
                    },
                  }}
                />
              ) : (
                <div></div>
              )}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default ListParteExpediente;
