import React from 'react';
import {

    CCol, 
    CCardBody,
    CButton,
    CLabel,
    CFormText,
    CCardFooter,
    CFormGroup,
    
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
//import ChartLineSimple from '../charts/ChartLineSimple';
import { urlserviciosweb } from '../conffile/config';
import { Hint } from 'react-autocomplete-hint';
import { useForm } from 'react-hook-form';

const ParteForm = (props) => {

    //-------------------Datos Expediente-----------------------------
    const [cudeexpediente,SetCudeExpediente ] = React.useState(0);
    
    React.useEffect(()=>{
        async function obtenerdatosexpediente(){
            const data = await fetch(`${urlserviciosweb}expediente/${props.idexpediente}`);
            if(data.status===200)
            {
                const datos = await data.json(); 
                SetCudeExpediente(datos.data.cude); 
            }
        }
        obtenerdatosexpediente();       
    },[props.idexpediente]); 


    //-------------------Lista de unidades-----------------------------    
    const [listaunidades, SetListaUnidades] = React.useState([]);   
    React.useEffect(()=>{
        async function obtenerunidad(){
            const data = await fetch(`${urlserviciosweb}unidad`);
            if(data.status ===200)
            {
                const listaunidades = await data.json();           
                SetListaUnidades(listaunidades.data); 
            }
        }
        obtenerunidad();       
    },[]);


    //-------------------Lista TipoParte--------------------------------
    const [listatipoparte, SetListaTipoParte] = React.useState([]);    
    React.useEffect(()=>{ 
        async function obtenertipoparte(){
            const data = await fetch(`${urlserviciosweb}tipoparte`);
            if(data.status === 200)
            {
                const listipopart = await data.json();           
                SetListaTipoParte(listipopart.data); 
            }
        }
        obtenertipoparte();       
    },[]);     
    //-------------------Lista Cargos--------------------------------
    const [listacargos, SetListaCargos] = React.useState([]);    
    React.useEffect(()=>{ 
        async function obtenercargos(){
            const data = await fetch(`${urlserviciosweb}cargo`);
            if(data.status === 200)
            {
                const listcargos = await data.json();           
                SetListaCargos(listcargos.data); 
            }
        }
        obtenercargos();       
    },[]); 


    //-------------------Array Autocomplete-----------------------------
    const [hintData, setHintData] = React.useState([])

    React.useEffect(()=>{
        async function getData(){
            const data = await fetch(`${urlserviciosweb}persona`);
            console.log(data);
            if(data.status === 200)
            {
                console.log(data);
                const personas = await data.json();            
                var hintArray = [];
                personas.data.map(a => hintArray.push({ label:`${a.nombre} ${a.paterno} ${a.materno}-${a.ci}`, id: a.idpersona,}))
                setHintData(hintArray)
            }
        }
        getData();       
    },[]);      

    //----------------Obtener Valores de Formulario Registro de Parte--------------
    const {register, errors, handleSubmit} = useForm();
    const [entradas, setentradas] = React.useState([]);

    const procesarFormularioparte = (data, e) => {
        //console.log(data);
        setentradas([
          ...entradas,
          data
        ])
        // limpiar campos        
        registrarParte(data);
        e.target.reset();        
    };

    async function registrarParte(datosFormulario){

        let obtIdentificadorPersona=0;

        for (let valores  of hintData){
            if (valores.label.toUpperCase()===datosFormulario.idpersona.toUpperCase()) {
                //console.log("El texto y el Label son iguales");
                //console.log(valores.id);
                obtIdentificadorPersona = valores.id
            }
        };
        datosFormulario.idpersona = obtIdentificadorPersona;
        datosFormulario.idexpediente = props.idexpediente;
        //console.log(datosFormulario);          

        const respuesta = await fetch(`${urlserviciosweb}parte`,{
            method:'POST',
            body:JSON.stringify(datosFormulario),
            headers:{
              'Content-Type': 'application/json; charset=utf-8',
            },           
        }); 

        const mensaje = await respuesta.json();
        alert(mensaje.message);
        
        //document.getElementById("formularioparte").reset();

    }    

 
    return (    
    <>

        <form  className="form-horizontal" id="formularioparte" onSubmit={handleSubmit(procesarFormularioparte)}  >
                            
            <CCardBody>

                <CFormGroup row> 
                    <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>CUDE Expediente:</strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">                                                                                                 
                        <strong>{cudeexpediente}</strong>
                    </CCol> 
                </CFormGroup> 

                <CFormGroup row> 
                    <CCol xs="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>Nombre de la Parte : <code>(*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">   

                        <Hint options={hintData} allowTabFill>
                            <input 
                                //className='input-with-hint'
                                type="text"         
                                style={{fontWeight:'bold'}}                                       
                                className="form-control my-7"
                                id="idpersona" 
                                name="idpersona"
                                ref={
                                    register({
                                        required: { value:true, message: 'Introduzca el nombre.'},
                                        pattern:  { value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ0-9\-\s]+$/i, message: 'Puede Introducir, letras, números, y el guio medio'}
                                    })
                                }  
                            />
                        </Hint>                                       
                        <CFormText>Ejem: Nombre PrimerApellido SegundoApellido</CFormText>
                        <span className="text-danger text-small d-block mb-2">
                            {errors?.idpersona?.message}
                        </span>
                                        
                    </CCol>
                    <CCol xs="12" md="2">                                                     
                        <CButton
                            size="sm"
                            color="danger"
                            title = "Registrar Nueva Persona"
                            to= {`/expediente/persona/form`}
                        >
                            <CIcon name="cil-people" />  
                        </CButton>
                    </CCol>                                    
                </CFormGroup> 


                <CFormGroup row> 
                    <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>Tipo Parte <code>(*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">                    
                        <select 
                            //custom 
                            name="idtipoparte" 
                            id="idtipoparte"  
                            className="form-control my-2"
                            ref={register({})}
                        >
                            <option value="0">Seleccion Tipo Parte.....</option>
                                {listatipoparte.map((element) => (        
                                    <option key={ element.idtipoparte} value={ element.idtipoparte}>{ element.descripcion}</option>
                                ))} 
                        </select>
                        <CFormText>Seleccione el tipo de la parte.</CFormText>
                    </CCol> 
                </CFormGroup>  

                <CFormGroup row> 
                    <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>Unidad<code> (*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">                    
                        <select 
                            //custom 
                            name="idunidad" 
                            id="idunidad"  
                            className="form-control my-2"
                            ref={register({})}
                        >
                            <option value="0">Seleccion unidad.....</option>
                                {listaunidades.map((element) => (        
                                <option key={ element.idunidad} value={ element.idunidad}>{ element.descripcion}</option>
                                ))} 
                        </select>
                        <CFormText>Seleccione la unidad de la parte.</CFormText>
                    </CCol> 
                </CFormGroup>  

                <CFormGroup row> 
                    <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label"><strong>Cargo<code> (*)</code></strong></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">                    
                        <select 
                            //custom 
                            name="idcargo" 
                            id="idcargo"  
                            className="form-control my-2"
                            ref={register({})}
                        >
                            <option value="0">Seleccion Cargo.....</option>
                                {listacargos.map((element) => (        
                                <option key={ element.idcargo} value={ element.idcargo}>{ element.descripcion}</option>
                                ))} 
                        </select>
                        <CFormText>Seleccione el cargo de la Parte.</CFormText>
                    </CCol> 
                </CFormGroup>  

                <CCardFooter>
                    <CButton type="submit"  size="sm" color="danger" ><CIcon name="cil-save" /> Guardar</CButton>   
                </CCardFooter>
                                                            
            </CCardBody>

        </form>

    </>
    )

}

    
export default ParteForm;