import React from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CFormGroup,
  CFormText,
  CLabel,
  CRow,
  CBreadcrumb,
  CListGroup,
  CListGroupItem,
  CTextarea,
} from "@coreui/react";

import CIcon from "@coreui/icons-react";
import { useForm } from "react-hook-form";
import { urlserviciosweb } from "../config";

const ExpedienteForm = () => {
  //------------------Lista tipo medidas precautorias-----------------
  const [listatipomedidasprecautorias, SetListaTipoMedidasPrecautorias] =
    React.useState([]);

  React.useEffect(() => {
    async function obtenertipomedidasprecuatorias() {
      const data = await fetch(`${urlserviciosweb}/tipomedidaprecautoria`);
      if (data.status === 200) {
        const listatipomedidasprecautorias = await data.json();
        SetListaTipoMedidasPrecautorias(listatipomedidasprecautorias.data);
      }
    }
    obtenertipomedidasprecuatorias();
  }, []);

  //--------------------Lista de unidades-----------------------
  const [listaunidades, SetListaUnidades] = React.useState([]);

  React.useEffect(() => {
    async function obtenerunidad() {
      const uni = await fetch(`${urlserviciosweb}/unidad`);
      if (uni.status === 200) {
        const lista = await uni.json();
        console.log(lista);
        SetListaUnidades(lista.data);
      }
    }
    obtenerunidad();
  }, []);

  //----------Enviando Datos de formulario------------------------

  const { register, errors, handleSubmit } = useForm();
  const [entradas, setentradas] = React.useState([]);

  async function onSubmit(data) {
    console.log(data);

    const respuesta = await fetch(`${urlserviciosweb}/expediente`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });

    const mensaje = await respuesta.json();
    alert(mensaje.message);
  }

  return (
    <>
      <CRow>
        <CCol xs="12" md="12">
          <CCard accentColor="success">
            <CCardHeader>
              <strong>FORMULARIO DE REGISTRO DE NUEVO EXPEDIENTE</strong>
            </CCardHeader>

            <CCardBody>
              <CBreadcrumb>
                <CListGroup accent>
                  <CListGroupItem accent="success">
                    <strong>DENUNCIA :</strong>
                  </CListGroupItem>
                </CListGroup>
              </CBreadcrumb>

              <CCard borderColor="success">
                <form
                  onSubmit={handleSubmit(onSubmit)}
                  encType="multipart/form-data"
                  className="form-horizontal"
                  id="formularioregistro"
                >
                  <CCardBody>
                    <CFormGroup row>
                      <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label">
                          <strong>
                            Tipo de Medida Precautoria <code>*</code>
                          </strong>
                        </CLabel>
                      </CCol>
                      <CCol xs="12" md="6">
                        <select
                          //custom
                          name="idtipomedidaprecautoria"
                          id="idtipomedidaprecautoria"
                          className="form-control my-2"
                          ref={register({
                            required: {
                              value: true,
                              message:
                                "Seleccione el tipo de medida precautoria.",
                            },
                            //validate: { idjuzgado: value => (value === "Seleccion unidad....." || "Seleccione Unidad")}
                          })}
                        >
                          {listatipomedidasprecautorias.map((element) => (
                            <option
                              key={element.idtipomedidaprecautoria}
                              value={element.idtipomedidaprecautoria}
                            >
                              {element.descripcion}
                            </option>
                          ))}
                        </select>
                        <CFormText>
                          Seleccione El tipo de medida Precautoria.
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.idjuzgado?.message}
                        </span>
                      </CCol>
                    </CFormGroup>

                    <CFormGroup row>
                      <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label">
                          <strong>
                            Unidad <code>*</code>
                          </strong>
                        </CLabel>
                      </CCol>
                      <CCol xs="12" md="6">
                        <select
                          //custom
                          name="idjuzgado"
                          id="idjuzgado"
                          className="form-control my-2"
                          ref={register({
                            required: {
                              value: true,
                              message: "Seleccione la unidad.",
                            },
                            //validate: { idjuzgado: value => (value === "Seleccion unidad....." || "Seleccione Unidad")}
                          })}
                        >
                          {listaunidades.map((element) => (
                            <option
                              key={element.idunidad}
                              value={element.idunidad}
                            >
                              {element.descripcion}
                            </option>
                          ))}
                        </select>
                        <CFormText>Seleccione la unidad de trabajo.</CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.idjuzgado?.message}
                        </span>
                      </CCol>
                    </CFormGroup>

                    <CFormGroup row>
                      <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label">
                          <strong>
                            Relato de los Hechos <code>*</code>
                          </strong>
                        </CLabel>
                      </CCol>
                      <CCol xs="12" md="6">
                        <CTextarea
                          id="relatohechos"
                          name="relatohechos"
                          className="form-control my-2"
                          innerRef={register({
                            required: {
                              value: true,
                              message: "Describa los Hechos",
                            },
                          })}
                        />
                        <CFormText></CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.relatohechos?.message}
                        </span>
                      </CCol>
                    </CFormGroup>

                    <CFormGroup row>
                      <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label">
                          <strong>
                            Fecha de Ingreso de la Denuncia <code>*</code>
                          </strong>
                        </CLabel>
                      </CCol>
                      <CCol xs="12" md="6">
                        <input
                          type="date"
                          id="fechahoraingresodenuncia"
                          name="fechahoraingresodenuncia"
                          placeholder="date"
                          className="form-control my-2"
                          ref={register({
                            required: {
                              value: true,
                              message:
                                "Seleccionar fecha de ingreso de la denuncia",
                            },
                          })}
                        />
                        <CFormText>
                          Introduzca la fecha de ingreso de denuncia (dia, mes,
                          año) Ejemplo: 31/11/2020.
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.fechahoraingresodenuncia?.message}
                        </span>
                      </CCol>
                    </CFormGroup>

                    <CFormGroup row>
                      <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label">
                          <strong>
                            Número de Fojas <code>*</code>
                          </strong>
                        </CLabel>
                      </CCol>
                      <CCol xs="12" md="6">
                        <input
                          type="number"
                          id="numerofojasdenuncia"
                          name="numerofojasdenuncia"
                          placeholder="Numero de Fojas......"
                          className="form-control my-2"
                          ref={register({
                            required: {
                              value: true,
                              message: "Introducir número de fojas",
                            },
                            pattern: {
                              value: /^[0-9]{1,8}$/i,
                              message:
                                "Introudzca números enteros, maximo de 5 dígitos",
                            },
                          })}
                        />
                        <CFormText>
                          Introduzca el Numero de fojas: Ejemplo; 4, 10, 1, 0
                          etc.
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.numerofojasdenuncia?.message}
                        </span>
                      </CCol>
                    </CFormGroup>

                    <CCardFooter>
                      <CButton type="submit" size="sm" color="success">
                        <CIcon name="cil-save" /> GUARDAR
                      </CButton>

                      <CButton
                        type="submit"
                        size="sm"
                        color="danger"
                        to="/expediente/expedientelist"
                      >
                        <CIcon name="cil-chevron-left" /> VOLVER
                      </CButton>
                    </CCardFooter>
                  </CCardBody>
                </form>
              </CCard>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default ExpedienteForm;
