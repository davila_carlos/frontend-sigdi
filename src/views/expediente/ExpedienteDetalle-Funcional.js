import React from 'react';
import {

    CWidgetBrand,  
    CCol, 
    CLink,
    CCard,
    CCardBody,
    CRow,
    CCardHeader,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CCallout,
    
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import ChartLineSimple from '../charts/ChartLineSimple';
import { useParams } from "react-router";
import ListParteExpediente from './parte/ListParteExpediente';
import ListPruebaExpediente from './prueba/ListPruebaExpediente';
import ParteForm from './parte/ParteForm';
import PruebaForm from './prueba/PruebaForm';
import { urlserviciosweb } from './conffile/config';

const ExpedienteDetalle = () => {

    const { idexpediente } = useParams();


    //---------Datos Expediente-----------------------    
    const [cudeexpediente,SetCudeExpediente ] = React.useState(0);    
    const [unidadexpediente,SetUnidadExpediente ] = React.useState('');    

    React.useEffect(()=>{
      async function obtenerdatosexpediente(){
        const data = await fetch(`${urlserviciosweb}expediente/${idexpediente}`);
        const datos = await data.json();           
        SetCudeExpediente(datos.data.cude); 
        SetUnidadExpediente(datos.data.unidad.nombreunidadsinaes);
      }
      obtenerdatosexpediente();       
    },[]);     


    //-------------Modal Registrar Parte----------------------------------------
    const [modalRegistrarParte, setModalRegistrarParte] = React.useState(false);
    const abriModalRegistrarParte=()=>{
        setModalRegistrarParte(true);
        document.getElementById("formularioparte").reset();
    }

    //-------------Modal Registrar Prueba----------------------------------------
    const [modalRegistrarPrueba, setModalRegistrarPrueba] = React.useState(false);
    const abriModalRegistrarPrueba=()=>{
        setModalRegistrarPrueba(true);
        document.getElementById("formularioprueba").reset();
    }

    return (    
    <>


        <CRow>

            <CCol sm="4" lg="4">        
                <CLink
                    size="sm"
                    color="success"
                    title = "Registrar o Asignar Parte"
                    //to= {`/expediente/persona/AsignarPartePersonaExpediente/${idexpediente}`}
                    onClick={()=>abriModalRegistrarParte()}
                >    
                    <CWidgetBrand
                        //rightHeader="12"
                        rightFooter="Registrar"
                        //leftHeader="3"
                        leftFooter="Parte"            
                        color="gradient-warning"
                    >
                        <CIcon
                            name="cil-people"
                            height="50"
                            //className="my-4"
                            />
                            <ChartLineSimple
                            className="position-absolute w-100 h-100"
                            backgroundColor="rgba(255,255,255,.1)"
                            dataPoints={[35, 23, 56, 22, 97, 23, 64]}
                            label="Registrar Parte"
                            labels="months"
                            />
                    </CWidgetBrand>
                </CLink> 
            </CCol>

            <CCol sm="4" lg="4">        
                <CLink
                    size="sm"
                    color="success"
                    title = "Registrar Prueba"
                    //to= {`/expediente/persona/AsignarPartePersonaExpediente/${idexpediente}`}
                    onClick={()=>abriModalRegistrarPrueba()}
                >    
                    <CWidgetBrand
                        //rightHeader="12"
                        rightFooter="Registrar"
                        //leftHeader="3"
                        leftFooter="Prueba"            
                        color="gradient-success"
                    >
                        <CIcon
                            name="cil-file"
                            height="50"
                            //className="my-4"
                            />
                            <ChartLineSimple
                            className="position-absolute w-100 h-100"
                            backgroundColor="rgba(255,255,255,.1)"
                            dataPoints={[35, 23, 56, 22, 97, 23, 64]}
                            label="Registrar Prueba"
                            labels="months"
                        />
                    </CWidgetBrand>
                </CLink> 
            </CCol>

            <CCol sm="4" lg="4">        
                <CLink
                    size="sm"
                    color="info"
                    title = "Registrar Prueba"
                    to= {`/expediente/caratulaexpediente/${idexpediente}`}
                    //to= {`/expediente/persona/list`}
                >    
                    <CWidgetBrand
                        //rightHeader="12"
                        rightFooter="imprimir"
                        //leftHeader="3"
                        leftFooter="Reporte"            
                        color="gradient-danger"
                    >
                        <CIcon
                            name="cil-print"
                            height="50"
                            //className="my-4"
                        >
                        
                        
                        </CIcon>

                        <ChartLineSimple
                            className="position-absolute w-100 h-100"
                            backgroundColor="rgba(255,255,255,.1)"
                            dataPoints={[35, 23, 56, 22, 97, 23, 64]}
                            label="Followers"
                            labels="months"
                        />
                    </CWidgetBrand>
                </CLink> 
            </CCol>

        </CRow>



        <CRow>
            <CCol>
                <CCard>
                    <CCardHeader>
                        <strong>VISTA DETALLADA DE EXPEDIENTE</strong>
                    </CCardHeader>
                    <CCardBody>
        
                        <CRow>
                            <CCol sm="6">
                                <CCallout color="success">
                                    <small className="text-muted">Unidad donde se encuentra el expediente</small>
                                    <br />
                                    <strong className="h6">{unidadexpediente}</strong>
                                </CCallout>
                            </CCol>
                            <CCol sm="6">
                                <CCallout color="danger">
                                    <small className="text-muted">Código de Expeciente (CUDE)</small>
                                    <br />
                                    <strong className="h6">{cudeexpediente}</strong>
                                </CCallout>
                            </CCol>
                        </CRow>

                        <CRow>
                            <CCol sm="12">
                            <ListParteExpediente idexpediente={idexpediente}></ListParteExpediente>
                            </CCol>
                        </CRow>

                        <CRow>
                        <CCol sm="12">
                            <ListPruebaExpediente idexpediente={idexpediente}></ListPruebaExpediente>      
                            </CCol>  
                        </CRow>



                    </CCardBody>
                </CCard>
            </CCol>
        </CRow>


        <CModal 
            show={modalRegistrarParte} 
            onClose={() => setModalRegistrarParte(!modalRegistrarParte)}
            color="warning"
            //size="sm"
            size="lg"
        >
            <CModalHeader closeButton>                
            </CModalHeader>

            <CModalBody>
                <CCard borderColor="secondary">    

                    <CCardHeader>   
                        <CRow>    
                            <CCol xs="8">  
                                <h6>FORMULARIO DE REGISTRO DE NUEVO PARTE</h6>
                            </CCol>                
                        </CRow>
                    </CCardHeader>
                    

                    <CCardBody>  
                        <ParteForm idexpediente={idexpediente}></ParteForm>
                    </CCardBody>
                </CCard>
            </CModalBody>

            <CModalFooter>
            </CModalFooter>
        </CModal>


        <CModal 
            show={modalRegistrarPrueba} 
            onClose={() => setModalRegistrarPrueba(!modalRegistrarPrueba)}
            color="success"
            //size="sm"
            size="lg"
        >
            <CModalHeader closeButton>                
            </CModalHeader>

            <CModalBody>
                <CCard borderColor="secondary">    

                    <CCardHeader>   
                        <CRow>    
                            <CCol xs="8">  
                                <h6>FORMULARIO DE REGISTRO DE NUEVA PRUEBA</h6>
                            </CCol>               
                        </CRow>
                    </CCardHeader>
                    
                    <CCardBody>  
                        <PruebaForm idexpediente={idexpediente}></PruebaForm>
                    </CCardBody>

                </CCard>
            </CModalBody>

            <CModalFooter>
            </CModalFooter>
        </CModal>

    </>
    )

}

    
export default ExpedienteDetalle