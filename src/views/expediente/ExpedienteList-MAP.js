import React from 'react'

import {
  //CBadge,
  CCard,
  CCardBody,
  //CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,

  CBreadcrumb,
  CListGroup,
  CListGroupItem,
  CInput,
  //CFormText,
  CNavbar,
  CForm,
  CNavbarNav,
  CNavLink
  
} from '@coreui/react'
import { element } from 'prop-types'

//import usersData from '../../../../users/UsersData';
//import usersData from  '../users/UsersData';

const getBadge = status => {
  switch (status) {
    case 'Active': return 'success'
    case 'Inactive': return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}
const fields = ['cude','relatohechos','unidad']
const idunidad = 10;
const urlservicioweb = `http://localhost:3000/api/expediente/juzgado/${idunidad}`;


const ExpedienteList = () => {

    const [listaexpedientes, SetListaExpedientes] = React.useState([])
    //const [unidad, setunidad] = React.useState('');

    React.useEffect(()=>{
        async function obtenerDatos(){
            const data = await fetch(urlservicioweb);
            const expe = await data.json();            
            SetListaExpedientes(expe.data); 
            
        }
        obtenerDatos();       
    },[]);    
    

    console.log('Datos EXPEDIENTE:');
    console.log(listaexpedientes);




    /*React.useEffect(()=>{
       fetch(`http://localhost:3000/api/expediente/juzgado/${idunidad}`)
       .then(response => response.json())
       .then(data => SetExpedientes(data.data));

    },[]);

    console.log(expedientes);*/


    /*React.useEffect(()=>{
        fetch('http://jsonplaceholder.typicode.com/users/')
        .then(response => response.json())
        .then(data => SetExpedientes(data));
 
    },[]);
 
     console.log(expedientes);*/

     return (
        <>
          <p>Data:</p>
          
            {listaexpedientes.map((element) => (
              
              <div className="CDataTable">
              <article className="card">
              <div className="card__data s-border s-radius-br s-radius-bl s-pxy-2">
              <h3 className="center">{ element.cude}</h3>
              <div className="s-main-center">{ element.unidad.sigla }</div>
              </div>
                     </article>
                </div>              
            ))}
          

          
        </>
      );
}

export default ExpedienteList
