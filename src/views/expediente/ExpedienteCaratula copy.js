import React from 'react';
import { useParams } from "react-router";
import ExpedienteReporte from './ExpedienteReporte';
//import ReactPDF from '@react-pdf/renderer';
//import ReactDOM from 'react-dom';
import { PDFViewer } from '@react-pdf/renderer';
import { PDFDownloadLink} from '@react-pdf/renderer';

 
//ReactDOM.render(<App />, document.getElementById('root'));*/


const ExpedienteCaratula = () => {

    const { idexpediente } = useParams();

    //console.log(idexpediente);

    //ReactPDF.render(<ExpedienteReporte />, `${__dirname}/example.pdf`);

    return (
        <>    
    
            <PDFViewer width="700" height="800"> 
                <ExpedienteReporte />
            </PDFViewer>
            <br/>
            <PDFDownloadLink document={<ExpedienteReporte idexpediente={idexpediente}/>} fileName={"FileName"}> 

                <button> Download </button> 

            </PDFDownloadLink>
        </>
    )    

}

export default ExpedienteCaratula
