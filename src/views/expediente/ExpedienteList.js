import React from "react";
import {
  CCard,
  CCardBody,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CBreadcrumb,
  CListGroup,
  CListGroupItem,
  CNavbar,
  CNavbarNav,
  CNavLink,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import fileDownload from "js-file-download";

import { urlserviciosweb } from "./conffile/config";

const ExpedienteList = () => {
  //-------------Lista Expedientes-------------------------------------
  const [listaexpedientes, SetListaExpedientes] = React.useState([]);

  React.useEffect(() => {
    async function obtenerDatos() {
      const data = await fetch(`${urlserviciosweb}expediente`);
      if (data.status === 200) {
        const expe = await data.json();
        SetListaExpedientes(expe.data);
      }
    }
    obtenerDatos();
  }, []);

  console.log(listaexpedientes);

  const seleccionarArchivo = (archivo) => {
    async function obtenerArchivoExpediente() {
      const respuesta = await fetch(
        `${urlserviciosweb}expediente/archivoexp/${archivo}`
      );
      const blob = await respuesta.blob();
      //console.log(blob);
      fileDownload(blob, archivo);
    }
    obtenerArchivoExpediente();
  };

  return (
    <>
      <CRow>
        <CCol>
          <CCard accentColor="success">
            <CCardBody>
              <CBreadcrumb>
                <CListGroup accent>
                  <CListGroupItem accent="warning">
                    <strong>DENUNCIAS</strong>
                  </CListGroupItem>
                </CListGroup>
              </CBreadcrumb>

              <CNavbar expandable="sm" color="">
                <CNavbarNav className="ml-auto">
                  <CNavLink to="/expediente/expedienteform">
                    <CButton color="success">NUEVA DENUNCIA</CButton>
                  </CNavLink>
                </CNavbarNav>
              </CNavbar>

              {listaexpedientes.length > 0 ? (
                <CDataTable
                  items={listaexpedientes}
                  fields={[
                    {
                      key: "cude",
                      label: "Código Unico de Expediente",
                      _style: { width: "20%" },
                      sorter: true,
                      filter: true,
                    },
                    //'unidad',
                    {
                      key: "unidad",
                      label: "Juzgado",
                      _style: { width: "20%" },
                      sorter: true,
                      filter: false,
                    },
                    //'fechahoraingresodenuncia',
                    {
                      key: "fechahoraingresodenuncia",
                      label: "Fecha de Registro de Denuncia",
                      _style: { width: "20%" },
                      sorter: true,
                      filter: false,
                    },
                    //'numerofojasdenuncia',
                    {
                      key: "numerofojasdenuncia",
                      label: "Número de fojas",
                      _style: { width: "20%" },
                      sorter: true,
                      filter: false,
                    },
                    // {
                    //   key: "relatohechos",
                    //   label: "Archivo Adjunto Relato de los hechos",
                    //   _style: { width: "20%" },
                    //   sorter: false,
                    //   filter: false,
                    // },
                    {
                      key: "verexpediente",
                      label: "",
                      _style: { width: "1%" },
                      sorter: false,
                      filter: false,
                    },
                  ]}
                  tableFilter={{
                    placeholder: "Palabra a Buscar...........",
                    label: "BUSCAR DENUNCIA :",
                  }}
                  hover
                  sorter
                  striped
                  bordered
                  size="sm"
                  itemsPerPage={10}
                  pagination
                  scopedSlots={{
                    unidad: (item) => <td>{item.unidad.descripcion}</td>,
                    fechahoraingresodenuncia: (item) => (
                      <td>{item.fechahoraingresodenuncia.split("T")[0]}</td>
                    ),
                    relatohechos: (item) => (
                      <td className="py-2">
                        <CButton
                          size="sm"
                          color="danger"
                          title={`Archivo Adjunto - ${item.relatohechos}`}
                          onClick={() => seleccionarArchivo(item.relatohechos)}
                        >
                          <CIcon name="cil-cloud-download" />
                        </CButton>
                      </td>
                    ),
                    verexpediente: (item, index) => {
                      return (
                        <td className="py-2">
                          <CButton
                            size="sm"
                            color="info"
                            title="Ver Detalle Expediente"
                            //variant="outline"    npm
                            to={`/expediente/detalle/${item.idexpediente}`}
                          >
                            <CIcon name="cil-file" />
                          </CButton>
                        </td>
                      );
                    },
                  }}
                />
              ) : (
                <div></div>
              )}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default ExpedienteList;
