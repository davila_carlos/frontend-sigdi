import React from 'react'
import {
  CCard,
  CCardBody,
  CCol,
  CRow,
  CButton,
  CCardFooter,
  CCardHeader,
  CFormGroup,
  CFormText,
  CLabel,
  CCallout,
  
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { useParams } from "react-router";
import { useForm } from 'react-hook-form';
import { urlserviciosweb } from '../conffile/config';
import DireccionesPersonaList from './DireccionesPersonaList';

const DireccionesPersona = ()=>{
    
    const { idparte } = useParams();

    //---------------Obteniendo Datos de la Parte--------------------------------------
    const [nombreparte, SetNombreParte] = React.useState(); 
    const [identificadorpersona, SetIdentificadorPersona] = React.useState();  
    const [identificadorexpediente, SetIdentificadorExpediente] = React.useState(); 
    React.useEffect(()=>{ 
    
        async function obtenerdatosparte(){
            const data = await fetch(`${urlserviciosweb}parte/${idparte}`);
            if(data.status ===200)
            {
                const datos = await data.json(); 
                //console.log(datos.data);
                let nombrecompleto = `${datos.data.persona.nombre} ${datos.data.persona.paterno} ${datos.data.persona.materno}`;
                SetNombreParte(nombrecompleto.toUpperCase());
                SetIdentificadorPersona(datos.data.persona.idpersona);
                SetIdentificadorExpediente(datos.data.idexpediente);
            }
        }
        obtenerdatosparte();
    
    },); 

    //---------Lista de tipo de direcciones-----------------------    
    const [listatipodireccion, SetListaTipoDirecciones] = React.useState([]);    

    React.useEffect(()=>{
        async function obtenerListaDirecciones(){
            const data = await fetch(`${urlserviciosweb}tipodireccion`);
            if(data.status === 200)
            {
                const listatipodireccion = await data.json();           
                SetListaTipoDirecciones(listatipodireccion.data); 
            }
        }
        obtenerListaDirecciones();       
    },[]); 


    //----------------Obtener Valores de Formulario Registro de Parte--------------
    const {register, errors, handleSubmit} = useForm();
    const [entradas, setentradas] = React.useState([]);

    const procesarFormularioDireccionPersona = (data, e) => {        
        //console.log(data);
        setentradas([
          ...entradas,
          data
        ])
        // limpiar campos        
        registrarDireccionesPersona(data);
        e.target.reset();        
    };   
    
    async function registrarDireccionesPersona(datosFormulario){      

        const respuesta = await fetch(`${urlserviciosweb}direccion`,{
            method:'POST',
            body:JSON.stringify(datosFormulario),
            headers:{
              'Content-Type': 'application/json; charset=utf-8',
            },           
        }); 

        const mensaje = await respuesta.json();
        alert(mensaje.message);
        
        document.getElementById("formulariodireccionpersona").reset();
    }      
    
  

    return (
        <>
        <CRow>
            <CCol>
                <CCard>
                    <CCardHeader>
                        <strong>DETALLE DE LAS DIRECCIONES DE LA PARTE</strong>
                    </CCardHeader>
                    <CCardBody>
        
                        <CRow>
                            <CCol sm="6">
                                <CCallout color="danger">
                                    <small className="text-muted">Nombre de la Parte Denunciada.</small>
                                    <br />
                                    <strong className="h6">{nombreparte}</strong>
                                </CCallout>
                            </CCol>
                        </CRow>

                        <CRow>
                            <CCol sm="12">
                                <DireccionesPersonaList idpersona={identificadorpersona}></DireccionesPersonaList>  
                            </CCol>
                        </CRow>

                        <CRow>
                            <CCol xs="12" md="12">
                                <CCard borderColor="success">

                                    <CCardHeader>          
                                        <strong>FORMULARIO DE REGISTRO DE DIRECCIONES DE LA PARTE</strong>
                                    </CCardHeader>
                                    <form  className="form-horizontal" id="formulariodireccionpersona" onSubmit={handleSubmit(procesarFormularioDireccionPersona)}  >                                
                                        <CCardBody>
                                            <input 
                                                type="hidden"
                                                id="idpersona" 
                                                name="idpersona" 
                                                value={identificadorpersona} 
                                                ref={register({})}
                                            />   
                                            <CFormGroup row>
                                                <CCol md="3">
                                                    <CLabel className="col-sm-10 col-form-label"><strong>Tipo de direccion:<code>(*)</code></strong></CLabel>
                                                </CCol>
                                                <CCol xs="12" md="7">
                                                    <select 
                                                        //custom 
                                                        name="idtipodireccion" 
                                                        id="idtipodireccion"  
                                                        className="form-control my-2"
                                                        ref={register({})}
                                                    >
                                                        <option value="0">Seleccionar tipo dirección.....</option>
                                                        {listatipodireccion.map((element) => (        
                                                            <option key={ element.idtipodireccion} value={ element.idtipodireccion}>{ element.descripcion}</option>
                                                        ))} 
                                                    </select>
                                                    <CFormText><strong>Seleccione el tipo de dirección.</strong></CFormText>
                                                </CCol> 
                                            </CFormGroup>                                 
                                            <CFormGroup row> 
                                                <CCol md="3">
                                                    <CLabel className="col-sm-10 col-form-label"><strong>Desccripción:<code>(*)</code></strong></CLabel>
                                                </CCol>
                                                <CCol xs="12" md="7">                    
                                                    <input 
                                                        type="text" 
                                                        id="descripcion" 
                                                        name="descripcion"                          
                                                        placeholder="Dirección......"
                                                        className="form-control my-2" 
                                                        ref={
                                                            register({  
                                                                required: { value:true, message: 'Introduzca la dirección'},                                                  
                                                                pattern:  { value: /^[.@,0-9A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i, message: 'Debe introducir solo letras,números,puntos y comas'}
                                                            })
                                                        }                                      
                                                    />
                                                    <CFormText>                                                
                                                        <strong>Ejem direccion Domicilio: <code>Calle No. 456, Zona Central</code>.</strong><br/>
                                                        <strong>Ejem direccion buzon: <code>correo@gmail.com</code></strong><br/>
                                                    </CFormText>
                                                    <span className="text-danger text-small d-block mb-2">
                                                        {errors?.descripcion?.message}
                                                    </span>
                                                </CCol>                  
                                            </CFormGroup>      

                                            <CCardFooter>
                                                <CButton type="submit"  size="sm" color="success" ><CIcon name="cil-save" /> Guardar</CButton>{ "   "}  
                                                <CButton size="sm" color="danger" to= {`/expediente/detalle/${identificadorexpediente}`} ><CIcon name="cil-chevron-left" /> Volver</CButton>    
                                            </CCardFooter>
                                        </CCardBody> 
                                    </form>
                                </CCard> 
                            </CCol>
                        </CRow>  
                        
                    </CCardBody>                    
                </CCard>
            </CCol>
        </CRow> 
        </>
    )

}

export default DireccionesPersona;