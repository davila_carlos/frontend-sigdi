import React from 'react'
import {
  //CCallout    ,
  //CBadge,
  CCard,
  CCardBody,
  //CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,

  CBreadcrumb,
  CListGroup,
  CListGroupItem,
  //CInput,
  //CFormText,
  //CNavbar,
  //CForm,
  //CNavbarNav,
  //CNavLink,

  //------
  CCardFooter,
  //CCardHeader,
  CFormGroup,
  CFormText,
  //CLabel,
  CLink,
  
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { useParams } from "react-router";

import { urlserviciosweb } from '../conffile/config';

//import { Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

const DireccionesPersona = ()=>{

    
    const { idpersona } = useParams();

    console.log("Entrando a Vista Direcciones Persona");
    console.log(idpersona);

    //const[existendireccion, SetExistenDirecciones] = React.useState(false);

    //---------Lista de direcciones persona-----------------------    
    const [listadireccionespersona, SetListaDireccionesPersona] = React.useState([]);    

    React.useEffect(()=>{
        async function obtenerdireccionespersona(){
            const data = await fetch(`${urlserviciosweb}direccion/persona/${idpersona}`);
            const listadireccionespersona = await data.json();    
            console.log(listadireccionespersona);       
            SetListaDireccionesPersona(listadireccionespersona.data); 
        }
        obtenerdireccionespersona();       
    },[]); 

    console.log('Lista de Direcciones de Persona:');
    console.log(listadireccionespersona);

    //---------Lista de tipo de direcciones-----------------------    
    const [listatipodireccion, SetListaTipoDirecciones] = React.useState([]);    

    React.useEffect(()=>{
        async function obtenerListaDirecciones(){
            const data = await fetch(`${urlserviciosweb}tipodireccion`);
            const listatipodireccion = await data.json();    
            console.log(listatipodireccion);       
            SetListaTipoDirecciones(listatipodireccion.data); 
        }
        obtenerListaDirecciones();       
    },[]); 

   

    return (
        <>
        <CRow>
            <CCol>
                <CCard accentColor="success">
    
                    <CCardBody>   
    
    
                        <CBreadcrumb>      
                            <CListGroup accent>
                                <CListGroupItem accent="warning">
                                    Formulario de Registro de Direcciones de Persona:                                                                                            
                                </CListGroupItem>
                            </CListGroup>
                        </CBreadcrumb> 

                        <CCard  borderColor="success" >
                            <form  id="formulariopersona" className="form-horizontal"  >
                                <CCardBody>
                                    <CFormGroup row>
                                    <CCol xs="12" md="4">
                                        <select 
                                            //custom 
                                            name="idtipodireccion" 
                                            id="idtipodireccion"  
                                            className="form-control my-2"
                                            //onChange={handleChange} 
                                        >
                                            <option value="0">Seleccionar tipo dirección.....</option>
                                            {listatipodireccion.map((element) => (        
                                                <option key={ element.idtipodireccion} value={ element.idtipodireccion}>{ element.descripcion}</option>
                                            ))} 
                                        </select>
                                        <CFormText>Seleccione el tipo de dirección.</CFormText>
                                        <span className="text-danger text-small d-block mb-2">
                                        
                                        </span>
                                    </CCol> 

                                    <CCol xs="12" md="4">                    
                                        <input 
                                        type="text" 
                                        id="descripcion" 
                                        name="descripcion"                          
                                        placeholder="Descripcion......"
                                        className="form-control my-2"
                                        /*ref={
                                            register({
                                                pattern:  { value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i, message: 'Introduzca letras'}
                                            })
                                        }*/  
                                        //onChange={handleChange}                                        
                                        />
                                        <CFormText>Introduzca la Descripción.</CFormText>
                                        <span className="text-danger text-small d-block mb-2">
                                        
                                        </span>
                                    </CCol>                  
                                    </CFormGroup>      

                                    <CCardFooter>
                                    <CButton type="submit"  size="sm" color="success" ><CIcon name="cil-save" /> Guardar</CButton>   
                                    </CCardFooter>
                                </CCardBody> 
                            </form>
                        </CCard>   

                        <CBreadcrumb>      
                            <CListGroup accent>
                                <CListGroupItem accent="warning">
                                    Lista de Direcciones de Persona:                                                                                            
                                </CListGroupItem>
                            </CListGroup>
                        </CBreadcrumb> 

                        <CDataTable
                            items={listadireccionespersona}
                            fields={
                                [
                                    {
                                        key: 'idtipodireccion',
                                        label: 'Tipo de Dirección',
                                        _style: { width: '20%' },
                                        sorter: false,
                                        filter: false
                                    },   
                                    {
                                        key: 'descripcion',
                                        label: 'Descripción de la Dirección',
                                        _style: { width: '20%' },
                                        sorter: false,
                                        filter: false
                                    },                                                                                                              
                                    {
                                        key: 'fechahoraregistro',
                                        label: 'Fecha Hora de Registro',
                                        _style: { width: '20%' },
                                        sorter: false,
                                        filter: false
                                    },  
                                    {
                                        key: 'actualizardireccion',
                                        label: '',
                                        _style: { width: '20%' },
                                        sorter: false,
                                        filter: false
                                    },                                                                            
                                    
                                ]
                            }                        
                            //tableFilter={{'placeholder':'Palabra a Buscar...........', 'label':'BUSCAR DENUNCIA :'}}
                            hover
                            sorter
                            striped
                            bordered
                            size="sm"
                            //footer=false
                            //itemsPerPage={10}
                            pagination
                            scopedSlots = {{
                                'idtipodireccion':
                                (item)=>(
                                    <td>                                
                                        {item.tipodireccion.descripcion}                                 
                                    </td>
                                ),  
                                'descripcion':
                                (item)=>(
                                    <td>                                
                                        {item.descripcion}                                 
                                    </td>
                                ),                                 
                                'fechahoraregistro':
                                (item)=>(
                                    <td>                                
                                        {item.fechahoraregistro.split('T')[0]}                                  
                                    </td>
                                ),                       
                                'actualizardireccion':
                                (item, index)=>{
                                    return (
                                        <td className="py-2">
                                            <CLink
                                                size="sm"
                                                color="secondary"
                                                title = "Editar Dirección"
                                                to= {`/expediente/persona/direcciones/${item.idpersona}`}
                                            >
                                                <CIcon name="cil-check" />  
                                           </CLink>
                                        </td>
                                    )
                                },
                            }}
                        />



                    </CCardBody>
                </CCard>
            </CCol>
        </CRow>





        

        </>
    )

}

export default DireccionesPersona;