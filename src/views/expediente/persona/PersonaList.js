import React from 'react'
import {
  //CCallout    ,
  //CBadge,
  CCard,
  CCardBody,
  //CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,

  CBreadcrumb,
  CListGroup,
  CListGroupItem,
  //CInput,
  //CFormText,
  CNavbar,
  //CForm,
  CNavbarNav,
  CNavLink,
  CLink,
  
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { urlserviciosweb } from '../conffile/config';

//const urlserviciosweb = `http://localhost:3000/api/`;

const PersonaList = ()=> {

    const [listapersonas, SetListaPersonas] = React.useState(null);    

    React.useEffect(()=>{
        async function obtenerListaPersonas(){
            const data = await fetch(`${urlserviciosweb}persona`);
            const personas = await data.json();            
            SetListaPersonas(personas.data); 
        }
        obtenerListaPersonas();       
    },[]);    
    

    console.log('Datos Persona:');
    console.log(listapersonas);


    return (
        <>
    
        <CRow>
            <CCol>
                <CCard accentColor="success">
    
                    <CCardBody>             
    
                        <CBreadcrumb>      
                            <CListGroup accent>
                                <CListGroupItem accent="warning">
                                    LISTA DE PERSONAS                                                                                            
                                </CListGroupItem>
    
                            </CListGroup>
                        </CBreadcrumb>                     
                        <CNavbar expandable="sm" color="" >                   
                            <CNavbarNav className="ml-auto">
                                <CNavLink to="/expediente/persona/form"><CButton color="success">NUEVA PERSONA</CButton></CNavLink>                         
                            </CNavbarNav>                 
                        </CNavbar>                                         
    
                        <CDataTable
                            items={listapersonas}
                            fields={
                                [
                                    'nombre',
                                    'paterno',
                                    'materno',   
                                    {
                                        key:'ci',  
                                        label: 'Cédula de Identidad',
                                        _style: { width: '20%'},
                                        sorter: true,
                                        filter: true
                                    },                                  
                                    //'complemento',
                                    'telefono',
                                    'celular',    
                                    'profesion',
                                    'ocupacion',
                                    {
                                        key: 'vertipodirecciones',
                                        label: '',
                                        _style: { width: '1%' },
                                        sorter: false,
                                        filter: false
                                    }                                   
                                ]
                            }                        
                            tableFilter={{'placeholder':'Palabra a Buscar...........', 'label':'BUSCAR:'}}
                            hover
                            sorter
                            striped
                            bordered
                            size="sm"
                            itemsPerPage={10}
                            pagination
                            scopedSlots = {{                                     
                                'ci':
                                (item)=>(
                                    <td>                                
                                        {item.ci} {item.complemento}                                 
                                    </td>
                                ),                                                                                        
                                'vertipodirecciones':
                                (item, index)=>{
                                    return (
                                        <td className="py-2">
                                            <CLink
                                                size="sm"
                                                color="secondary"
                                                title = "Ver Direcciones de Persona"
                                                //to= {`/expediente/persona/direcciones/${item.idpersona}`}
                                            >
                                                <CIcon name="cil-file" />  
                                           </CLink>
                                        </td>
                                    )
                                },                             
                            }}
                        />
                    </CCardBody>
                </CCard>
            </CCol>
        </CRow>
            
        </>
      )   

}





export default PersonaList;