import React from 'react';
import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    //CCollapse,
    //CDropdownItem,
    //CDropdownMenu,
    //CDropdownToggle,
    //CFade,
    //CForm,
    CFormGroup,
    CFormText,
    //CValidFeedback,
    //CInvalidFeedback,
    //CTextarea,
    //CInput,
    //CInputFile,
    //CInputCheckbox,
    //CInputRadio,
    //CInputGroup,
    //CInputGroupAppend,
    //CInputGroupPrepend,
    //CDropdown,
    //CInputGroupText,
    //CLabel,
    //CSelect,
    CRow,
    CBreadcrumb,
    //CBreadcrumbItem,
    CListGroup,
    CListGroupItem     
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { useForm } from 'react-hook-form';

import { urlserviciosweb } from '../conffile/config';

//class PersonaForm extends React.Component{

//const urlservicioweb = `http://localhost:3000/api/expediente/juzgado/${idunidad}`;
//const urlserviciosweb = `http://localhost:3000/api/`;

const PersonaForm =() =>{
//const PersonaForm = () => {

  //const [collapsed, setCollapsed] = React.useState(true);
  //const [showElements, setShowElements] = React.useState(true);
  //const inputnombre = React.useRef('nombre');

  /*const onButtonClick= ()=>{
    console.log("REcienfo ");
    //inputnombre.current.focus();
   // console.log(inputnombre);

  }*/


  //-----------Validacion de formulario-------------------------------

  const {register, errors, handleSubmit} = useForm();

  const [entradas, setentradas] = React.useState([]);

  const procesarFormulariopersona = (data, e) => {
    //console.log(data);
    setentradas([
      ...entradas,
      data
    ])
    // limpiar campos
    registrarPersona(data);
    e.target.reset();
  }
    

  /*const initialFormData = Object.freeze({
    nombre: '',
    paterno:'',
    materno:'',
    ci: '',
    complemento:'',
    idexpedido:'',
    edad:0,
    telefono:'',
    celular:'',
    profesion:'',
    ocupacion:'',
    genero:'',
    fechanacimiento: ''    
  });*/


  //const [formData, updateFormData] = React.useState(initialFormData);

   
  /*const handleChange=(event)=>{
    //console.log(event.target.value);
    updateFormData({
      ...formData,
      [event.target.name]: event.target.value.trim()
    });
  };*/

  async function registrarPersona(data){

    console.log("DATOS VALIDADOS");
    console.log(data);

    /*//evento.preventDefault();
    console.log("DAtos de Formulario");    
    console.log(JSON.stringify(formData));

    //fetch('http://localhost:3000/api/persona ',{
    fetch(`${urlserviciosweb}persona `,{
      method:'POST',
      body:JSON.stringify(formData),
      headers:{
        'Content-Type': 'application/json; charset=utf-8',
        'Access-Control-Allow-Origin': '*',
      },           
    }).then(r=>r.json())
    .then(response => console.log('Succes',response))  
    .catch(error => console.error('Error',error));*/

    //let formularioRegistroPersona = document.getElementById('formulariopersona');   
    //let formData = new FormData(formularioRegistroPersona);

    /*const data = new FormData();
    const nombre = document.querySelector('input[name="nombre"]').value;
    const paterno = document.querySelector('input[name="paterno"]').value;
    const materno = document.querySelector('input[name="materno"]').value;
    const ci = document.querySelector('input[name="ci"]').value;
    const complemento = document.querySelector('input[name="complemento"]').value;  
    const idexpedido = document.querySelector('#idexpedido').value;  
    const ocupacion = document.querySelector('input[name="ocupacion"]').value;    
    const profesion = document.querySelector('input[name="profesion"]').value;       
    const genero = document.querySelector('#genero').value;     
    const fechanacimiento = document.querySelector('input[name="fechanacimiento"]').value;
    const edad = document.querySelector('input[name="edad"]').value;
    const telefono = document.querySelector('input[name="telefono"]').value;
    const celular = document.querySelector('input[name="celular"]').value;
    
    console.log("datos del formulario");
    console.log(nombre);
    console.log(paterno);    
    console.log(materno);    
    console.log(ci);       
    console.log(complemento);       
    console.log(idexpedido);           
    console.log(ocupacion);               
    console.log(profesion);                   
    console.log(genero);                       
    console.log(fechanacimiento);                           
    console.log(edad);
    console.log(telefono);    
    console.log(celular);        
    
    
    data.append("nombre", document.querySelector('input[name="nombre"]').value);
    data.append("paterno", paterno);
    data.append("materno", materno);
    data.append("ci", ci);
    data.append("complemento", complemento);
    data.append("idexpedido", idexpedido);    
    data.append("ocupacion", ocupacion);        
    data.append("profesion", profesion);            
    data.append("genero", genero);                
    data.append("fechanacimiento", fechanacimiento);                    
    data.append("edad", edad);                        
    data.append("telefono", telefono);                            
    data.append("celular", celular);                                


    console.log("SEND JSON FORMULARIO");
    console.log(JSON.stringify(data));*/
    
    const respuesta = await fetch(`${urlserviciosweb}persona`,{
      method:'POST',
      body:JSON.stringify(data),
      //body:formData,
      //body:data,
      headers:{
        'Content-Type': 'application/json; charset=utf-8',
        //'Content-Type': 'multipart/form-data',
        //'Access-Control-Allow-Origin': '*',
        //'type': 'formData',
      },           
    });
    const mensaje = await respuesta.json();
    alert(mensaje.message); 
    


  };

  //-------------Lista Expedido-------------------------------------

  const [listaexpedido, SetListaExpedido] = React.useState([]);    
  React.useEffect(()=>{
      async function obtenerExpedidos(){
          const data = await fetch(`${urlserviciosweb}expedido`);
          const listexp = await data.json();           
          SetListaExpedido(listexp.data); 
      }
      obtenerExpedidos();       
  },[]);   
  
  console.log('Datos EXPEDIDO:................');
  console.log(listaexpedido);
  //-------------Lista TipoDireccion-------------------------------------

  const [listatipodireccion, SetListaTipoDireccion] = React.useState([]);    
  React.useEffect(()=>{
      async function obtenertipodireccion(){
          const data = await fetch(`${urlserviciosweb}tipodireccion`);
          const listipodir = await data.json();           
          SetListaTipoDireccion(listipodir.data); 
      }
      obtenertipodireccion();       
  },[]);   
  
  console.log('Datos tipo direccion:');
  console.log(listatipodireccion);

  //-------------Lista TipoParte-------------------------------------

  const [listatipoparte, SetListaTipoParte] = React.useState([]);    
  React.useEffect(()=>{ 
      async function obtenertipoparte(){
          const data = await fetch(`${urlserviciosweb}tipoparte`);
          const listipopart = await data.json();           
          SetListaTipoParte(listipopart.data); 
      }
      obtenertipoparte();       
  },[]);   
  
  console.log('Datos tipo parte:');
  console.log(listatipoparte); 
  
  

  return (
    <>
    <CRow>

      <CCol xs="12" md="12">
        <CCard accentColor="success">

          <CCardHeader>          
            FORMULARIO DE REGISTRO 
          </CCardHeader>

          <CCardBody>            
 
              <CBreadcrumb>                  
                <CListGroup accent>
                  <CListGroupItem accent="success">INFORMACION GENERAL DE LA PERSONA</CListGroupItem>
                </CListGroup>
              </CBreadcrumb> 

              <CCard  borderColor="success" >

                <form  id="formulariopersona" className="form-horizontal" onSubmit={handleSubmit(procesarFormulariopersona)}  >
                
                  <CCardBody>
                    <CFormGroup row>
                      <CCol xs="12" md="4">                    
                        <input  
                          type="text" 
                          id="nombre" 
                          name="nombre"                          
                          placeholder="Nombres......"  
                          className="form-control my-2"
                          ref={
                            register({
                                required: { value:true, message: 'Introduzca el nombre.'},
                                pattern:  { value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i, message: 'Debe introducir solo letras'}
                            })
                          } 
                          //value={this.state.nombre}   
                          //onChange={this.handleChange}                                    
                          //onChange={handleChange}
                        />
                        <CFormText>Introduzca nombres: Ejemplo; Adela, Maria.<code>(*)</code></CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.nombre?.message}
                        </span>
                      </CCol>

                      <CCol xs="12" md="4">                    
                        <input
                          type="text" 
                          id="paterno" 
                          name="paterno"                          
                          placeholder="Primer Apellido......"
                          className="form-control my-2"
                          ref={
                            register({
                                pattern:  { value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i, message: 'Introduzca letras'}
                            })
                          }                          
                          //onChange={handleChange}    
                          
                        />
                        <CFormText>Introduzca el apellido paterno: Ejemplo; Sanchez, Padilla.</CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.paterno?.message}
                        </span>
                      </CCol> 

                      <CCol xs="12" md="4">                    
                        <input 
                          type="text" 
                          id="materno" 
                          name="materno"                          
                          placeholder="Segundo Apellido......"
                          className="form-control my-2"
                          ref={
                            register({
                                pattern:  { value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i, message: 'Introduzca letras'}
                            })
                          }  
                          //onChange={handleChange}                                        
                        />
                        <CFormText>Introduzca el apellido materno: Ejemplo; Sanchez, Padilla.</CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.materno?.message}
                        </span>
                      </CCol>                  
                    </CFormGroup>  

                    <CFormGroup row>                 
                      <CCol xs="12" md="4">                    
                        <input
                          type="text" 
                          id="ci" 
                          name="ci" 
                          placeholder="Cédula de Identidad......"  
                          className="form-control my-2"
                          ref={
                            register({
                                required: { value:true, message: 'Introduzca la cédula de identidad.'},
                                pattern:  { value: /^[0-9]{1,8}$/i, message: 'Introduzca números enteros, maximo de 8 dígitos'}
                            })
                          }  
                          //onChange={handleChange} 
                        />
                        <CFormText>Introduzca el numero de la cédula de identidad : Ejemplo; 1234567.<code>(*)</code></CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.ci?.message}
                        </span>
                      </CCol>                  
                      <CCol xs="12" md="4">                    
                        <input  
                          type="text" 
                          id="complemento" 
                          name="complemento"                          
                          placeholder="Complemento......"
                          className="form-control my-2"
                          ref={
                            register({
                                pattern:  { value: /^[0-9a-zA-Z]{1,2}$/i, message: 'Introduzca números o letras, maximo de 2 dígitos'}
                            })
                          }  
                          //onChange={handleChange} 
                        />
                        <CFormText>Introduzca el complemento de la cédula Identidad: Ejemplo; L9, P5.</CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.complemento?.message}
                        </span>
                      </CCol>  
                      <CCol xs="12" md="4">
                        <select 
                          //custom 
                          name="idexpedido" 
                          id="idexpedido"  
                          className="form-control my-2"
                          //onChange={handleChange} 
                        >
                          <option value="0">Seleccionar lugar expedido.....</option>
                          {listaexpedido.map((element) => (        
                            <option key={ element.idexpedido} value={ element.idexpedido}>{ element.lugarexpedido}</option>
                          ))} 
                        </select>
                        <CFormText>Seleccione el lugar donde fue expedido la cédula
                          
                          
                          
                          
                           de identidad.</CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.idexpedido?.message}
                        </span>
                      </CCol>               
                    </CFormGroup>

                    <CFormGroup row>                
                      <CCol xs="12" md="4">                    
                        <input
                          type="text" 
                          id="ocupacion" 
                          name="ocupacion"                           
                          placeholder="Ocupación......" 
                          className="form-control my-2"
                          ref={
                            register({
                                pattern:  { value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i, message: 'Introduzca letras'}
                            })
                          }   
                          //onChange={handleChange} 
                        />
                        <CFormText>Introduzca la Ocupación: Ejemplo; Juez, Administración, etc.</CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.ocupacion?.message}
                        </span>
                      </CCol>                   
                      <CCol xs="12" md="4">                    
                        <input  
                          type="text" 
                          id="profesion" 
                          name="profesion" 
                          placeholder="Profesión......"
                          className="form-control my-2"
                          ref={
                            register({
                                pattern:  { value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i, message: 'Introduzca letras'}
                            })
                          }   
                          //onChange={handleChange}  
                        />
                        <CFormText>Introduzca la profesión : Ejemplo; Abogado(a), Administrador(ra) .</CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.profesion?.message}
                        </span>
                      </CCol>                
                      <CCol xs="12" md="4">
                        <select 
                          //custom 
                          name="genero" 
                          id="genero" 
                          className="form-control my-2"
                          //onChange={handleChange}  
                        >
                          <option value="0">Seleccionar Género.......</option>
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                        </select>
                        <CFormText>Seleccione el Género.</CFormText>
                      </CCol>               
                    </CFormGroup>   

                    <CFormGroup row>
                      <CCol xs="12" md="6">
                        <input 
                          type="date" 
                          id="fechanacimiento" 
                          name="fechanacimiento" 
                          placeholder="date"  
                          className="form-control my-2"
                          //onChange={handleChange}   
                        />
                        <CFormText>Introduzca la fecha de nacimiento (dia, mes, año): Ejemplo; 31/11/1980..</CFormText>
                      </CCol>
                      <CCol xs="12" md="6">                    
                        <input  
                          type="text" ls
                          id="edad" 
                          name="edad" 
                          placeholder="Edad......"     
                          className="form-control my-2"
                          ref={
                            register({
                                required: { value:true, message: 'Introduzca la edad.'},
                                pattern:  { value: /^[0-9]{1,3}$/i, message: 'Introduzca números enteros, maximo de 3 dígitos'}
                            })
                          }  
                          //onChange={handleChange}  
                        />
                        <CFormText>Introduzca la edad: Ejemplo; 65, 34..<code>(*)</code></CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.edad?.message}
                        </span>
                      </CCol>                 
                    </CFormGroup>                           
                 
                    <CFormGroup row>
                        <CCol xs="12" md="6">
                          <input  
                            type="text" 
                            id="telefono" 
                            name="telefono" 
                            placeholder="Telefono fijo......"    
                            className="form-control my-2"
                            ref={
                              register({
                                  pattern:  { value: /^[0-9-]{1,8}$/i, message: 'Introduzca números enteros, se permite el signo "-"'}
                              })
                            } 
                            //onChange={handleChange}  
                          />
                          <CFormText>Introduzca el teléfono fijo: Ejemplo; 64-58965 </CFormText>
                          <span className="text-danger text-small d-block mb-2">
                          {errors?.telefono?.message}
                        </span>
                      </CCol>
                      <CCol xs="12" md="6">                    
                        <input  
                            type="text" 
                            id="celular" 
                            name="celular" 
                            placeholder="Telefono Celular......"  
                            className="form-control my-2"
                            ref={
                              register({
                                  required: { value:true, message: 'Introduzca el teléfono celular.'},
                                  pattern:  { value: /^[0-9]{1,8}$/i, message: 'Debe introducirse números enteros, maximo de 8 dígitos'}
                              })
                            } 
                            //onChange={handleChange} 
                        />
                        <CFormText>Introduzca el teléfono celular: Ejemplo; 72812345.<code>(*)</code></CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.celular?.message}
                        </span>
                      </CCol>                                       
                    </CFormGroup>

                    <CCardFooter>
                      <CButton type="submit"  size="sm" color="success" ><CIcon name="cil-save" /> Guardar</CButton>   
                    </CCardFooter>
                  </CCardBody> 

                </form>


              </CCard>         

             


           

          </CCardBody>

        </CCard>    

      </CCol>
    </CRow>   
    
    </>
  )
}

export default PersonaForm;