import React from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CFormGroup,
  CFormText,
  CRow,
  CBreadcrumb,
  CListGroup,
  CListGroupItem,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useForm } from "react-hook-form";

import { urlserviciosweb } from "../conffile/config";

const PersonaForm = () => {
  //-------------Lista Expedido-------------------------------------
  const [listaexpedido, SetListaExpedido] = React.useState([]);
  React.useEffect(() => {
    async function obtenerExpedidos() {
      const data = await fetch(`${urlserviciosweb}expedido`);
      const listexp = await data.json();
      SetListaExpedido(listexp.data);
    }
    obtenerExpedidos();
  }, []);

  console.log("lista EXPEIDDO");
  console.log(listaexpedido);

  //-------------Lista TipoDireccion-------------------------------------
  /*const [listatipodireccion, SetListaTipoDireccion] = React.useState([]);
  React.useEffect(()=>{
      async function obtenertipodireccion(){
          const data = await fetch(`${urlserviciosweb}tipodireccion`);
          const listipodir = await data.json();
          SetListaTipoDireccion(listipodir.data);
      }
      obtenertipodireccion();
  },[]);   */

  //-------------Lista TipoParte-------------------------------------
  /*const [listatipoparte, SetListaTipoParte] = React.useState([]);
  React.useEffect(()=>{
      async function obtenertipoparte(){
          const data = await fetch(`${urlserviciosweb}tipoparte`);
          const listipopart = await data.json();
          SetListaTipoParte(listipopart.data);
      }
      obtenertipoparte();
  },[]);   */

  const { register, errors, handleSubmit } = useForm();
  const [entradas, setentradas] = React.useState([]);

  const procesarFormulariopersona = (data, e) => {
    console.log(data);
    setentradas([...entradas, data]);
    // limpiar campos
    registrarPersona(data);
    e.target.reset();
  };

  async function registrarPersona(data) {
    if (data.ci === "") {
      data.ci = 0;
    }
    /*if(data.idexpedido==0)
    {
      data.idexpedido = 10;
    } */
    if (data.fechanacimiento === "") {
      data.fechanacimiento = "1000-01-01";
    }
    if (data.edad === "") {
      data.edad = 0;
    }
    if (data.celular === "") {
      data.celular = 0;
    }
    //console.log(data);

    const respuesta = await fetch(`${urlserviciosweb}persona`, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
    });
    const mensaje = await respuesta.json();
    alert(mensaje.message);

    document.getElementById("formulariopersona").reset();
  }

  return (
    <>
      <CRow>
        <CCol xs="12" md="12">
          <CCard accentColor="success">
            <CCardHeader>
              <strong>FORMULARIO DE REGISTRO</strong>
            </CCardHeader>

            <CCardBody>
              <CBreadcrumb>
                <CListGroup accent>
                  <CListGroupItem accent="success">
                    <strong>INFORMACION GENERAL DE LA PERSONA</strong>
                  </CListGroupItem>
                </CListGroup>
              </CBreadcrumb>

              <CCard borderColor="success">
                <form
                  id="formulariopersona"
                  className="form-horizontal"
                  onSubmit={handleSubmit(procesarFormulariopersona)}
                >
                  <CCardBody>
                    <CFormGroup row>
                      <CCol xs="12" md="4">
                        <input
                          type="text"
                          id="nombre"
                          name="nombre"
                          placeholder="Nombres......"
                          className="form-control my-2"
                          ref={register({
                            required: {
                              value: true,
                              message: "Introduzca el nombre.",
                            },
                            pattern: {
                              value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i,
                              message: "Debe introducir solo letras",
                            },
                          })}
                        />
                        <CFormText>
                          <strong>
                            Introduzca nombres: Ejemplo; Adela, Maria.
                            <code>(*)</code>
                          </strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.nombre?.message}
                        </span>
                      </CCol>

                      <CCol xs="12" md="4">
                        <input
                          type="text"
                          id="paterno"
                          name="paterno"
                          placeholder="Primer Apellido......"
                          className="form-control my-2"
                          ref={register({
                            pattern: {
                              value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i,
                              message: "Introduzca letras",
                            },
                          })}
                        />
                        <CFormText>
                          <strong>
                            Introduzca el apellido paterno: Ejemplo; Sanchez,
                            Padilla
                          </strong>
                          .
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.paterno?.message}
                        </span>
                      </CCol>

                      <CCol xs="12" md="4">
                        <input
                          type="text"
                          id="materno"
                          name="materno"
                          placeholder="Segundo Apellido......"
                          className="form-control my-2"
                          ref={register({
                            pattern: {
                              value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i,
                              message: "Introduzca letras",
                            },
                          })}
                        />
                        <CFormText>
                          <strong>
                            Introduzca el apellido materno: Ejemplo; Sanchez,
                            Padilla.
                          </strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.materno?.message}
                        </span>
                      </CCol>
                    </CFormGroup>

                    <CFormGroup row>
                      <CCol xs="12" md="4">
                        <input
                          type="text"
                          id="ci"
                          name="ci"
                          placeholder="Cédula de Identidad......"
                          className="form-control my-2"
                          ref={register({
                            //required: { value:true, message: 'Introduzca la cédula de identidad.'},
                            pattern: {
                              value: /^[0-9]{1,8}$/i,
                              message:
                                "Introduzca números enteros, maximo de 8 dígitos",
                            },
                          })}
                        />
                        <CFormText>
                          <strong>
                            Introduzca el numero de la cédula de identidad :
                            Ejemplo; 1234567.<code>(*)</code>
                          </strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.ci?.message}
                        </span>
                      </CCol>
                      <CCol xs="12" md="4">
                        <input
                          type="text"
                          id="complemento"
                          name="complemento"
                          placeholder="Complemento......"
                          className="form-control my-2"
                          ref={register({
                            pattern: {
                              value: /^[0-9a-zA-Z]{1,2}$/i,
                              message:
                                "Introduzca números o letras, maximo de 2 dígitos",
                            },
                          })}
                        />
                        <CFormText>
                          <strong>
                            Introduzca el complemento de la cédula Identidad:
                            Ejemplo; L9, P5.
                          </strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.complemento?.message}
                        </span>
                      </CCol>
                      <CCol xs="12" md="4">
                        <select
                          //custom
                          name="idexpedido"
                          id="idexpedido"
                          className="form-control my-2"
                          ref={register({})}
                        >
                          <option value="10">
                            Seleccionar lugar expedido.....
                          </option>
                          {listaexpedido.map((element) => (
                            <option
                              key={element.idexpedido}
                              value={element.idexpedido}
                            >
                              {element.lugarexpedido}
                            </option>
                          ))}
                        </select>
                        <CFormText>
                          <strong>
                            Seleccione el lugar donde fue expedido la cédulade
                            identidad.<code>(*)</code>
                          </strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.idexpedido?.message}
                        </span>
                      </CCol>
                    </CFormGroup>

                    <CFormGroup row>
                      <CCol xs="12" md="4">
                        <input
                          type="text"
                          id="ocupacion"
                          name="ocupacion"
                          placeholder="Ocupación......"
                          className="form-control my-2"
                          ref={register({
                            pattern: {
                              value: /^[.A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i,
                              message: "Introduzca letras",
                            },
                          })}
                        />
                        <CFormText>
                          <strong>
                            Introduzca la Ocupación: Ejemplo; Juez,
                            Administración, etc.
                          </strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.ocupacion?.message}
                        </span>
                      </CCol>
                      <CCol xs="12" md="4">
                        <input
                          type="text"
                          id="profesion"
                          name="profesion"
                          placeholder="Profesión......"
                          className="form-control my-2"
                          ref={register({
                            pattern: {
                              value: /^[.A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i,
                              message: "Introduzca letras",
                            },
                          })}
                        />
                        <CFormText>
                          <strong>
                            Introduzca la profesión : Ejemplo; Abogado(a),
                            Administrador(ra) .
                          </strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.profesion?.message}
                        </span>
                      </CCol>
                      <CCol xs="12" md="4">
                        <select
                          //custom
                          name="genero"
                          id="genero"
                          className="form-control my-2"
                          ref={register({})}
                        >
                          <option value="0">Seleccionar Género.......</option>
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                        </select>
                        <CFormText>
                          <strong>Seleccione el Género.</strong>
                        </CFormText>
                      </CCol>
                    </CFormGroup>

                    <CFormGroup row>
                      <CCol xs="12" md="6">
                        <input
                          type="date"
                          id="fechanacimiento"
                          name="fechanacimiento"
                          placeholder="date"
                          className="form-control my-2"
                          ref={register({})}
                        />
                        <CFormText>
                          <strong>
                            Introduzca la fecha de nacimiento (dia, mes, año):
                            Ejemplo; 31/11/1980.
                          </strong>
                        </CFormText>
                      </CCol>
                      <CCol xs="12" md="6">
                        <input
                          type="text"
                          id="edad"
                          name="edad"
                          placeholder="Edad......"
                          className="form-control my-2"
                          ref={register({
                            //required: { value:true, message: 'Introduzca la edad.'},
                            pattern: {
                              value: /^[0-9]{1,3}$/i,
                              message:
                                "Introduzca números enteros, maximo de 3 dígitos",
                            },
                          })}
                        />
                        <CFormText>
                          <strong>Introduzca la edad: Ejemplo; 65, 34.</strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.edad?.message}
                        </span>
                      </CCol>
                    </CFormGroup>

                    <CFormGroup row>
                      <CCol xs="12" md="6">
                        <input
                          type="text"
                          id="telefono"
                          name="telefono"
                          placeholder="Telefono fijo......"
                          className="form-control my-2"
                          ref={register({
                            pattern: {
                              value: /^[0-9-]{1,8}$/i,
                              message:
                                'Introduzca números enteros, se permite el signo "-"',
                            },
                          })}
                        />
                        <CFormText>
                          <strong>
                            Introduzca el teléfono fijo: Ejemplo; 64-58965
                          </strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.telefono?.message}
                        </span>
                      </CCol>
                      <CCol xs="12" md="6">
                        <input
                          type="text"
                          id="celular"
                          name="celular"
                          placeholder="Telefono Celular......"
                          className="form-control my-2"
                          ref={register({
                            //required: { value:true, message: 'Introduzca el teléfono celular.'},
                            pattern: {
                              value: /^[0-9]{1,8}$/i,
                              message:
                                "Debe introducirse números enteros, maximo de 8 dígitos",
                            },
                          })}
                        />
                        <CFormText>
                          <strong>
                            Introduzca el teléfono celular: Ejemplo; 72812345.
                          </strong>
                        </CFormText>
                        <span className="text-danger text-small d-block mb-2">
                          {errors?.celular?.message}
                        </span>
                      </CCol>
                    </CFormGroup>

                    <CCardFooter>
                      <CButton type="submit" size="sm" color="success">
                        <CIcon name="cil-save" /> Guardar
                      </CButton>{" "}
                      {}
                      <CButton
                        type="submit"
                        size="sm"
                        color="danger"
                        to="/expediente/expedientelist"
                      >
                        <CIcon name="cil-chevron-left" /> CANCELAR
                      </CButton>
                    </CCardFooter>
                  </CCardBody>
                </form>
              </CCard>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default PersonaForm;
