import React from 'react';
import {

    CCol, 
    CCard,
    CCardBody,
    CDataTable,
    CRow,
    CCardHeader,
    //CButton,    
    
} from '@coreui/react';
//import CIcon from '@coreui/icons-react';
import { urlserviciosweb } from '../conffile/config';


const DireccionesPersonaList = (props) => {

    //-------------Obtener Partes de Expediente-------------------------------------
    const [listadireccionespersona, SetListaDireccionesPersona] = React.useState([]);   
    React.useEffect(()=>{ 

        async function obtenerdireccionespersona(){
            const data = await fetch(`${urlserviciosweb}direccion/persona/${props.idpersona}`);
            if(data.status === 200)
            {

                const listdirper = await data.json();  
                SetListaDireccionesPersona(listdirper.data);
            }                                  
        }

        const aux = setInterval(() => {
            obtenerdireccionespersona();       
        }, 1000);       
        return () => clearInterval(aux);

    },); 


    return (
        <>
           
            <CRow >
                <CCol xs="12" md="12">
                    <CCard borderColor="warning"> 

                        <CCardHeader>
                            <strong>DIRECCIONES </strong>
                        </CCardHeader>

                        <CCardBody> 

                            { listadireccionespersona.length >0?(

                                <CDataTable
                                    items={listadireccionespersona}
                                    fields={
                                        [
                                            {
                                                key:'tipodireccion',  
                                                label: 'Tipo de Dirección',
                                                _style: { width: '20%'},
                                                sorter: false,
                                                filter: false
                                            }, 
                                            {
                                                key:'descripcion',  
                                                label: 'Descripcion',
                                                _style: { width: '20%'},
                                                sorter: false,
                                                filter: false
                                            },                                                                                                    
                                        ]
                                    }                        
                                    //tableFilter={{'placeholder':'Palabra a Buscar...........', 'label':'BUSCAR DENUNCIA :'}}
                                    //dark
                                    hover
                                    sorter
                                    striped
                                    bordered
                                    size="sm"
                                    itemsPerPage={10}
                                    //pagination
                                    scopedSlots = {{
                                        'tipodireccion':
                                        (item)=>(
                                            <td>                                
                                                {item.tipodireccion.descripcion}                             
                                            </td>
                                        ),    
                                        'descripcion':
                                        (item)=>(
                                            <td>                                
                                                {item.descripcion}                             
                                            </td>
                                        ),                                                                                                    
                                    }}
                                />
                                
                            ):( <div></div>)}  
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>   
                   
        </>
    )

}
    
export default DireccionesPersonaList;