import React from 'react';
import {

    CInputRadio,
    CWidgetBrand,  
    CCol, 
    CLink,
    //CCallout    ,
    //CBadge,
    CCard,
    CCardBody,
    //CCardHeader,

    //CDataTable,
    CRow,
    CButton,
    CLabel,
    //CBreadcrumb,
    //CListGroup,
    //CListGroupItem,
    //CInput,
    CFormText,
    //CNavbar,
    //CForm,
    //CNavbarNav,
    //CNavLink,
  
    //------
    CCardFooter,
    CCardHeader,
    CFormGroup,
    //CProgress,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    //CModalTitle,
    
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import ChartLineSimple from '../charts/ChartLineSimple';
import { urlserviciosweb } from './conffile/config';
import { useParams } from "react-router";
import { Hint } from 'react-autocomplete-hint';
import { useForm } from 'react-hook-form';
//import { Redirect} from 'react-router-dom';

import ListParteExpediente from './parte/ListParteExpediente';

const ExpedienteDetalle = () => {

    const { idexpediente } = useParams();


    //-------------Modal Registrar Parte----------------------------------------
    const [modalRegistrarParte, setModalRegistrarParte] = React.useState(false);
    const abriModalRegistrarParte=()=>{
        setModalRegistrarParte(true);
        document.getElementById("formularioparte").reset();
    }

    //-------------Modal Registrar Prueba----------------------------------------
    const [modalRegistrarPrueba, setModalRegistrarPrueba] = React.useState(false);
    const abriModalRegistrarPrueba=()=>{
        setModalRegistrarPrueba(true);
        document.getElementById("formularioprueba").reset();
    }



    //-------------------Lista de unidades-----------------------------    
    const [listaunidades, SetListaUnidades] = React.useState([]);   
    React.useEffect(()=>{
        async function obtenerunidad(){
            const data = await fetch(`${urlserviciosweb}unidad`);
            const listaunidades = await data.json();           
            SetListaUnidades(listaunidades.data); 
        }
        obtenerunidad();       
    },[]);


    //-------------------Lista TipoParte--------------------------------
    const [listatipoparte, SetListaTipoParte] = React.useState([]);    
    React.useEffect(()=>{ 
        async function obtenertipoparte(){
            const data = await fetch(`${urlserviciosweb}tipoparte`);
            const listipopart = await data.json();           
            SetListaTipoParte(listipopart.data); 
        }
        obtenertipoparte();       
    },[]);     

    //-------------------Lista de Tipo Prueba-----------------------------    
    const [listatipoprueba, SetListaTipoPrueba] = React.useState([]);   
    React.useEffect(()=>{
        async function obtenertipoprueba(){
            const data = await fetch(`${urlserviciosweb}tipoprueba`);
            const listatipoprueba = await data.json();   
            SetListaTipoPrueba(listatipoprueba.data); 
        }
        obtenertipoprueba();       
    },[]);

    //console.log(listatipoprueba);




    //-------------------Array Autocomplete-----------------------------
    const [hintData, setHintData] = React.useState([])

    React.useEffect(()=>{
        async function getData(){
            const data = await fetch(`${urlserviciosweb}persona`);
            const personas = await data.json();            
            var hintArray = [];
            personas.data.map(a => hintArray.push({ label:`${a.nombre} ${a.paterno} ${a.materno}-${a.ci}`, id: a.idpersona,}))
            setHintData(hintArray)
            //console.log(hintArray.length);
        }
        getData();       
    },[]);      



    //----------------Obtener Valores de Formulario Registro de Parte--------------
//    const {register,errors, handleSubmit} = useForm();

    const {register, errors, handleSubmit} = useForm();
    const [entradas, setentradas] = React.useState([]);

    const procesarFormularioparte = (data, e) => {
        console.log("OBT DATOS PARTE");
        console.log(data);
        setentradas([
          ...entradas,
          data
        ])
        // limpiar campos        
        //registrarParte(data);
        e.target.reset();        
    };

    async function registrarParte(datosFormulario){

        console.log("Datos enviados de formulario");
        console.log(datosFormulario);

        let obtIdentificadorPersona=0;

        hintData.map((valores)=>{ 
            if(valores.label.toUpperCase()===datosFormulario.idpersona.toUpperCase())
            {
                //console.log("El texto y el Label son iguales");
                console.log(valores.id);
                obtIdentificadorPersona = valores.id
            }
        });
        datosFormulario.idpersona = obtIdentificadorPersona;
        datosFormulario.idexpediente = idexpediente;
        console.log(datosFormulario);          

        const respuesta = await fetch(`${urlserviciosweb}parte`,{
            method:'POST',
            body:JSON.stringify(datosFormulario),
            headers:{
              'Content-Type': 'application/json; charset=utf-8',
            },           
        }); 

        const mensaje = await respuesta.json();

        alert(mensaje.message);

        
        setModalRegistrarParte(false);

        document.getElementById("formularioparte").reset();
        
        //window.location.reload(true); //-------------------sirve recarga toda la pagina
        
        
        //return( vv
        //    <Redirect to="/expediente/expedienteform"/>
        //)        
        //history.push(`/expediente/detalle/${idexpediente}`);
        //window.location = `/expediente/detalle/${idexpediente}`;
    }    

    //----------------Obtener Valores de Formulario Registro de Prueba--------------
    //const {registe} = useForm();
    const [entradasprueba, setentradasPrueba] = React.useState([]);

    const procesarFormularioPrueba = (datos, e) => {
        console.log("DATOS DE FORM PRUEBA");
        console.log(datos);
        setentradasPrueba([
          ...entradasprueba,
          datos
        ])
        // limpiar campos        
        //registrarPrueba(datos);
        e.target.reset();     
    }; 

    /*async function registrarPrueba(){

        //evento.preventDefault();
        
      
        let myForm = document.getElementById('formularioprueba');   
        let formData = new FormData(myForm);
  
        //console.log(formData);
      
        //const respuesta = await fetch(`${urlserviciosweb}expediente `,{
        //  method:'POST',
        //  //body:JSON.stringify(data),
        //  body:formData,
        //  headers:{
        //    //'Content-Type': 'application/json; charset=utf-8',
        //    //'Content-Type': 'multipart/form-data',
        //    //'Access-Control-Allow-Origin': '*',
        //    //'type': 'formData',
        //  },           
        //});
        //const mensaje = await respuesta.json();
        //alert(mensaje.message);
  
        
        //.then(r=>r.json())
        ////.then(response => alert(`Success:${JSON.stringify(response)}`))  
        //.then((response) => {alert(`${response.message}`);}) 
        //.catch(error => alert('Error',error));
    }; */  
    
    




 
    return (
    
    <>

        <CRow>

            <CCol sm="3" lg="3">        
                <CLink
                    size="sm"
                    color="success"
                    title = "Registrar o Asignar Parte"
                    //to= {`/expediente/persona/AsignarPartePersonaExpediente/${idexpediente}`}
                    onClick={()=>abriModalRegistrarParte()}
                >    
                    <CWidgetBrand
                        //rightHeader="12"
                        rightFooter="Registrar"
                         //leftHeader="3"
                        leftFooter="Parte"            
                        color="gradient-warning"
                    >
                        <CIcon
                            name="cil-people"
                            height="50"
                            //className="my-4"
                            />
                            <ChartLineSimple
                            className="position-absolute w-100 h-100"
                            backgroundColor="rgba(255,255,255,.1)"
                            dataPoints={[35, 23, 56, 22, 97, 23, 64]}
                            label="Registrar Parte"
                            labels="months"
                            />
                    </CWidgetBrand>
                </CLink> 
            </CCol>

            <CCol sm="3" lg="3">        
                <CLink
                    size="sm"
                    color="success"
                    title = "Registrar Prueba"
                    //to= {`/expediente/persona/AsignarPartePersonaExpediente/${idexpediente}`}
                    onClick={()=>abriModalRegistrarPrueba()}
                >    
                    <CWidgetBrand
                        //rightHeader="12"
                        rightFooter="Registrar"
                        //leftHeader="3"
                        leftFooter="Prueba"            
                        color="gradient-success"
                    >
                        <CIcon
                            name="cil-file"
                            height="50"
                            //className="my-4"
                            />
                            <ChartLineSimple
                            className="position-absolute w-100 h-100"
                            backgroundColor="rgba(255,255,255,.1)"
                            dataPoints={[35, 23, 56, 22, 97, 23, 64]}
                            label="Registrar Prueba"
                            labels="months"
                        />
                    </CWidgetBrand>
                </CLink> 
            </CCol>

            <CCol sm="3" lg="3">        
                <CLink
                    size="sm"
                    color="info"
                    title = "Registrar Prueba"
                    to= {`/expediente/persona/AsignarPartePersonaExpediente/${idexpediente}`}
                >    
                    <CWidgetBrand
                        //rightHeader="12"
                        rightFooter="imprimir"
                        //leftHeader="3"
                        leftFooter="Reporte"            
                        color="gradient-info"
                    >
                        <CIcon
                            name="cil-print"
                            height="50"
                            //className="my-4"
                            />
                            <ChartLineSimple
                            className="position-absolute w-100 h-100"
                            backgroundColor="rgba(255,255,255,.1)"
                            dataPoints={[35, 23, 56, 22, 97, 23, 64]}
                            label="Followers"
                            labels="months"
                        />
                    </CWidgetBrand>
                </CLink> 
            </CCol>

        </CRow>



        <CModal 
            show={modalRegistrarPrueba} 
            onClose={() => setModalRegistrarPrueba(!modalRegistrarPrueba)}
            color="success"
            //size="sm"
            size="lg"
        >
            <CModalHeader closeButton>                
            </CModalHeader>

            <CModalBody>
                <CCard borderColor="secondary">    

                    <CCardHeader>   
                        <CRow>    
                            <CCol xs="8">  
                                <h6>FORMULARIO DE REGISTRO DE NUEVA PRUEBA</h6>
                            </CCol>   
                            {/*<CCol xs="3">  
                                <CButton
                                    size="sm"
                                    color="info"
                                    title="Registrar Nueva Persona"                  
                                    to= {`/expediente/persona/form`}
                                >
                                Nueva Persona 
                                </CButton>
                            </CCol> */}              
                        </CRow>
                    </CCardHeader>
                    

                    <CCardBody>  

                        <form  
                            className="form-horizontal" id="formularioprueba" onSubmit={handleSubmit(procesarFormularioPrueba)}  
                        >
                            
                            <CCardBody>

                                <CFormGroup row> 
                                    <CCol md="4">
                                        <CLabel className="col-sm-10 col-form-label">Identificador Expediente:<code>(*)</code></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">                                                                                                 
                                        {idexpediente}
                                    </CCol> 
                                </CFormGroup> 


                                <CFormGroup row> 
                                    <CCol xs="4">
                                        <CLabel className="col-sm-10 col-form-label">Seleccionar Tipo Prueba: <code>(*)</code></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">   
                                        <select 
                                            //custom 
                                            name="idtipoprueba" 
                                            id="idtipoprueba"  
                                            className="form-control my-2"
                                            //onChange={handleChange} 
                                            //ref={register}
                                            ref={register({})}
                                            >
                                            <option value="0">Seleccionar tipo Prueba.....</option>
                                            {listatipoprueba.map((element) => (        
                                                <option key={ element.idtipoprueba} value={ element.idtipoprueba}>{ element.descripcion}</option>
                                            ))} 
                                        </select>                                     
                                        <CFormText>Ejem: Grabación Multimedia, Documento, etc</CFormText>
                                        
                                    </CCol>                                   
                                </CFormGroup> 


                                <CFormGroup row> 
                                    <CCol md="4">
                                        <CLabel className="col-sm-10 col-form-label">Numero de fojas<code>(*)</code> </CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">                    
                                    <input
                                        type="text" 
                                        id="numerofojas" 
                                        name="numerofojas" 
                                        placeholder="Número de fojas......"  
                                        className="form-control my-2"
                                        ref={register({})}
                                        /*ref={
                                            register({
                                                //required: { value:true, message: 'Debe Introducir el número de fojas.' },
                                                pattern:  { value:  /^[0-9]{1,8}$/i, message: 'Introduzca números enteros, maximo de 5 dígitos'},
                                                
                                            })
                                        } */
                                    />
                                        <CFormText>Ejem: 1,2,3</CFormText>
                                        <span className="text-danger text-small d-block mb-2">
                                            {errors?.numerofojas?.message}
                                        </span>
                                    </CCol> 
                                </CFormGroup> 
                                
                                <CFormGroup row> 
                                    <CCol md="4">
                                        <CLabel className="col-sm-10 col-form-label">Adjuntar Archivo de las Pruebas Presentadas (pdf, doc, docx) :<code>(*)</code> </CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">                    
                                        <input 
                                            type="file" 
                                            id="ubicacionfisicadocumento" 
                                            name="ubicacionfisicadocumento" 
                                            className="form-control my-2"
                                            //ref={register}
                                            
                                            /*ref={
                                               register({
                                                    //required: { value:true, message: 'Adjuntar Archivo de Relato de los Hechos' }
                                                })
                                            } */                    
                                        />
                                        <CFormText>Seleccione el archivo a adjuntar: </CFormText>
                                        <span className="text-danger text-small d-block mb-2">
                                            {errors?.ubicacionfisicadocumento?.message}
                                        </span>
                                    </CCol> 
                                </CFormGroup> 
s            

                                <CCardFooter>
                                    <CButton type="submit"  size="sm" color="success" ><CIcon name="cil-save" /> Guardar</CButton>   
                                </CCardFooter>
                                                            
                            </CCardBody>

                        </form>

                    </CCardBody>
                </CCard>
            </CModalBody>

            <CModalFooter>
            </CModalFooter>
        </CModal>








        <CModal 
            show={modalRegistrarParte} 
            onClose={() => setModalRegistrarParte(!modalRegistrarParte)}
            color="warning"
            //size="sm"
            size="lg"
        >
            <CModalHeader closeButton>                
            </CModalHeader>

            <CModalBody>
                <CCard borderColor="secondary">    

                    <CCardHeader>   
                        <CRow>    
                            <CCol xs="8">  
                                <h6>FORMULARIO DE REGISTRO DE NUEVO PARTE</h6>
                            </CCol>   
                            {/*<CCol xs="3">  
                                <CButton
                                    size="sm"
                                    color="info"
                                    title="Registrar Nueva Persona"                  
                                    to= {`/expediente/persona/form`}
                                >
                                Nueva Persona 
                                </CButton>
                            </CCol> */}              
                        </CRow>
                    </CCardHeader>
                    

                    <CCardBody>  

                        <form  className="form-horizontal" id="formularioparte" onSubmit={handleSubmit(procesarFormularioparte)}  >
                            
                            <CCardBody>

                                <CFormGroup row> 
                                    <CCol md="4">
                                        <CLabel className="col-sm-10 col-form-label">Identificador Expediente:<code>(*)</code></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">                                                                                                 
                                        {idexpediente}
                                    </CCol> 
                                </CFormGroup> 

                                <CFormGroup row> 
                                    <CCol xs="4">
                                        <CLabel className="col-sm-10 col-form-label">Nombre de la Parte : <code>(*)</code></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">   

                                        <Hint options={hintData} allowTabFill>
                                            <input 
                                                className='input-with-hint'
                                                //placeholder="Introducir nombre"
                                                type="text"         
                                                style={{fontWeight:'bold'}}                                       
                                                className="form-control my-7"
                                                id="idpersona" 
                                                name="idpersona"
                                                //ref={register}
                                                ref={
                                                    register({
                                                        required: { value:true, message: 'Introduzca el nombre.'},
                                                        pattern:  { value: /^[A-Za-zÁÉÍÓÚáéíóúñÑ0-9\-\ ]+$/i, message: 'Puede Introducir, letras, números, y el guio medio'}
                                                    })
                                                }  
                                            />
                                        </Hint>                                       
                                        <CFormText>Ejem: Nombre PrimerApellido SegundoApellido</CFormText>
                                        <span className="text-danger text-small d-block mb-2">
                                            {errors?.idpersona?.message}
                                        </span>
                                        
                                    </CCol>
                                    <CCol xs="12" md="2">                                                     
                                        <CButton
                                            size="sm"
                                            color="success"
                                            title = "Registrar Nueva Persona"
                                            to= {`/expediente/persona/form`}
                                        >
                                            <CIcon name="cil-people" />  
                                       </CButton>
                                    </CCol>                                    
                                </CFormGroup> 

                                <CFormGroup row> 
                                    <CCol md="4">
                                        <CLabel className="col-sm-10 col-form-label">Unidad <code>(*)</code></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">                    
                                        <select 
                                            //custom 
                                            name="idunidad" 
                                            id="idunidad"  
                                            className="form-control my-2"
                                            //ref={register}
                                            ref={register({
                                                require : true,
                                            })}
                                        >
                                            <option value="0">Seleccion unidad.....</option>
                                            {listaunidades.map((element) => (        
                                                <option key={ element.idunidad} value={ element.idunidad}>{ element.nombreunidadsinaes}</option>
                                            ))} 
                                        </select>
                                        <CFormText>Seleccione la unidad de trabajo de la Parte.</CFormText>
                                    </CCol> 
                                </CFormGroup>    
                                <CFormGroup row> 
                                    <CCol md="4">
                                        <CLabel className="col-sm-10 col-form-label">Tipo Parte <code>(*)</code></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">                    
                                        <select 
                                            //custom 
                                            name="idtipoparte" 
                                            id="idtipoparte"  
                                            className="form-control my-2"
                                            //ref={register}
                                            ref={register({})}
                                        >
                                            <option value="0">Seleccion Tipo Parte.....</option>
                                            {listatipoparte.map((element) => (        
                                                <option key={ element.idtipoparte} value={ element.idtipoparte}>{ element.descripcion}</option>
                                            ))} 
                                        </select>
                                        <CFormText>Seleccione el tipo de la parte.</CFormText>
                                    </CCol> 
                                </CFormGroup>  

                                <CCardFooter>
                                    <CButton type="submit"  size="sm" color="success" ><CIcon name="cil-save" /> Guardar</CButton>   
                                </CCardFooter>
                                                            
                            </CCardBody>

                        </form>

                    </CCardBody>
                </CCard>
            </CModalBody>

            <CModalFooter>
            </CModalFooter>
        </CModal>



        <div id="listapartesexpediente">
            <ListParteExpediente idexpediente={idexpediente}></ListParteExpediente>
        </div>    




        



    </>

    )

}

    
export default ExpedienteDetalle