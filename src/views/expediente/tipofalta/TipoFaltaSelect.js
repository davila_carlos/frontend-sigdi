import React from 'react';
import {

    CCol, 
    CCardBody,
    CButton,
    CLabel,
    CFormText,
    CCardFooter,
    CFormGroup,
    
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
//import ChartLineSimple from '../charts/ChartLineSimple';
import { urlserviciosweb } from '../conffile/config';
import { useForm } from 'react-hook-form';
import { useParams } from "react-router";

const FaltaParteForm = (props) => {

    const { idparte } = useParams();

    console.log("identificador de la parte");
    console.log(idparte);


    //-------------------Lista de tipos de Falta-----------------------------    
    const [listatipofalta, SetListaTipoFalta] = React.useState([]);   
    React.useEffect(()=>{
        async function obtenerTipoFaltas(){
            const data = await fetch(`${urlserviciosweb}tipofalta`);
            const lista = await data.json();  
            console.log(lista);         
            SetListaTipoFalta(lista.data); 
        }
        obtenerTipoFaltas();       
    },[]);

    console.log(listatipofalta);    


    

    //---------------identificador del tipo de falta--------------------------------
    const [identificadorTipoFalta, SetIdentificadorTipoFalta] = React.useState(0);
    console.log("identificador del tipofalta");
    console.log(identificadorTipoFalta);

    //---------------List de Faltas segun el Tipo de Falta---------------------------    
    const [listafalta, SetListaFalta] = React.useState([]);   
    React.useEffect(()=>{
        async function obtenerFaltas(){
            const data = await fetch(`${urlserviciosweb}falta/tipofalta/${identificadorTipoFalta}`);
            const lista = await data.json();  
            console.log(lista);         
            SetListaFalta(lista.data); 
        }
        obtenerFaltas();       
    },[]);

    console.log("Lista de Faltas");
    console.log(listafalta);    


    return (    
    <>

        <form  className="form-horizontal" id="formulariofaltaparte" onSubmit={handleSubmit(procesarFormularioFaltaParte)}  >
                            
            <CCardBody>

                <CFormGroup row> 
                    <CCol md="4">
                        <CLabel className="col-sm-10 col-form-label">Identificador Parte:<code>(*)</code></CLabel>
                    </CCol>
                    <CCol xs="12" md="5">                                                                                                 
                        {props.idparte}
                    </CCol> 
                </CFormGroup> 

                <CFormGroup row> 
                    <CCol xs="12" md="4">
                        <select 
                          //custom 
                          name="" 
                          id="idtipofalta"  
                          className="form-control my-2"
                          //ref={register({})}
                          onChange={e => SetIdentificadorTipoFalta(e.target.value)} 
                        >
                          <option value="0">Seleccionar el tipo de la falta.....</option>
                          {listatipofalta.map((element) => (        
                            <option key={ element.idtipofalta} value={ element.idtipofalta}>{ element.descripcion}</option>
                          ))} 
                        </select>
                        <CFormText><strong>Seleccione el tipo de la falta.<code>(*)</code></strong></CFormText>
                    </CCol> 
                </CFormGroup> 

                {identificadorTipoFalta !=0 ?(
                <CFormGroup row> 
                    <CCol xs="12" md="4">
                        <select 
                          //custom 
                          name="" 
                          id="idtipofalta"  
                          className="form-control my-2"
                          //ref={register({})}
                          //onChange={e => SetIdentificadorTipoFalta(e.target.value)} 
                        >
                          <option value="0">Seleccionar la falta.....</option>
                          {listafalta.map((element) => (        
                            <option key={ element.idfalta} value={ element.idfalta}>{ element.descripcion}</option>
                          ))} 
                        </select>
                        <CFormText><strong>Seleccione la falta.<code>(*)</code></strong></CFormText>
                    </CCol> 
                </CFormGroup>                 
                ):(
                    <div></div>
                )} 

 

                <CCardFooter>
                    <CButton type="submit"  size="sm" color="danger" ><CIcon name="cil-save" /> Guardar</CButton>   
                </CCardFooter>
                                                            
            </CCardBody>

        </form>

    </>
    )

}

    
export default FaltaParteForm;