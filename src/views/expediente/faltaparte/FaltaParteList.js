import React from 'react';
import {

    CCol, 
    CCard,
    CCardBody,
    CDataTable,
    CRow,
    CCardHeader,
    //CButton,    
    
} from '@coreui/react';
//import CIcon from '@coreui/icons-react';
import { urlserviciosweb } from '../conffile/config';


const ListParteExpediente = (props) => {

    //-------------Obtener Partes de Expediente-------------------------------------
    const [listafaltasParte, SetListaFaltasParte] = React.useState([]);   
    React.useEffect(()=>{ 

        async function obtenerfaltasparte(){

            const data = await fetch(`${urlserviciosweb}faltaparte/${props.idparte}`);
            if(data.status ===200)
            {
                const listfaltaparte = await data.json();  
                SetListaFaltasParte(listfaltaparte.data);
            }                        
        }

        const faltaTimeChange = setInterval(() => {
            obtenerfaltasparte();       
        }, 1000);       
        return () => clearInterval(faltaTimeChange);

    },); 





    return (
        <>
           
           
            <CRow >
                <CCol xs="12" md="12">
                    <CCard borderColor="warning"> 

                        <CCardHeader>
                            <strong>FALTAS DE LA PARTES </strong>
                        </CCardHeader>

                        <CCardBody> 
                            { listafaltasParte.length>0?(
                            <CDataTable
                                items={listafaltasParte}
                                fields={
                                    [
                                        {
                                            key:'tipofalta',  
                                            label: 'Tipo Falta',
                                            _style: { width: '20%'},
                                            sorter: false,
                                            filter: false
                                        }, 
                                        {
                                            key:'falta',  
                                            label: 'Falta',
                                            _style: { width: '20%'},
                                            sorter: false,
                                            filter: false
                                        },    
                                        {
                                            key:'descripciondenuncia',  
                                            label: 'Descripcion de la Denuncia',
                                            _style: { width: '20%'},
                                            sorter: false,
                                            filter: false
                                        },                                                                                                   
                                    ]
                                }                        
                                //tableFilter={{'placeholder':'Palabra a Buscar...........', 'label':'BUSCAR DENUNCIA :'}}
                                //dark
                                hover
                                sorter
                                striped
                                bordered
                                size="sm"
                                itemsPerPage={10}
                                //pagination
                                scopedSlots = {{
                                    'tipofalta':
                                    (item)=>(
                                        <td>                                
                                            {item.faltum.idtipofalta===1? "Grave": item.faltum.idtipofalta===2?"Leve": item.faltum.idtipofalta===3?"Gravisima":"-"}                             
                                        </td>
                                    ),
                                    'falta':
                                    (item)=>(
                                        <td>                                
                                            {item.faltum.descripcion}                             
                                        </td>
                                    ),    
                                    'descripciondenuncia':
                                    (item)=>(
                                        <td>                                
                                            {item.descripciondenuncia}                             
                                        </td>
                                    ),                                                                                                    
                                }}
                            />
                            ):(<div></div>)} 
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>   
                    
        </>
    )

}
    
export default ListParteExpediente;