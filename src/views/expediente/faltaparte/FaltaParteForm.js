import React from 'react';
import {

    CCol, 
    CCardBody,
    CButton,
    CLabel,
    CFormText,
    CCardFooter,
    CFormGroup,
    CCard,
    //CDataTable,
    CCallout,
    CRow,
    CCardHeader,
    //CBreadcrumb,
    //CListGroup,
    //CListGroupItem,    
    
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
//import ChartLineSimple from '../charts/ChartLineSimple';
import { urlserviciosweb } from '../conffile/config';
import { useForm } from 'react-hook-form';
import { useParams } from "react-router";
import FaltaParteList from "../faltaparte/FaltaParteList";

const FaltaParteForm = (props) => {

    const { idparte } = useParams();

    //---------------Obteniendo Datos de la Parte--------------------------------------
    const [nombreparte, SetNombreParte] = React.useState(); 
    const [identificadorexpediente, SetIdentificadorExpediente] = React.useState();  
    React.useEffect(()=>{ 
    
        async function obtenerdatosparte(){
            const data = await fetch(`${urlserviciosweb}parte/${idparte}`);
            if(data.status ===200)
            {
                const datos = await data.json(); 
                let nombrecompleto = `${datos.data.persona.nombre} ${datos.data.persona.paterno} ${datos.data.persona.materno}`;
                SetNombreParte(nombrecompleto.toUpperCase());
                SetIdentificadorExpediente(datos.data.idexpediente);
            }
        }
        obtenerdatosparte();    
    },); 

    //-------------------Lista de tipos de Falta-----------------------------    
    const [listatipofalta, SetListaTipoFalta] = React.useState([]);   
    React.useEffect(()=>{
        async function obtenerTipoFaltas(){
            const data = await fetch(`${urlserviciosweb}tipofalta`);
            if(data.status === 200)
            {
                const lista = await data.json();          
                SetListaTipoFalta(lista.data); 
            }
        }
        obtenerTipoFaltas();       
    },[]);
   

    //---------------List de Faltas segun el Tipo de Falta---------------------------    
    const [listafalta, SetListaFalta] = React.useState([]);    

    const handleChange=(event)=>{
        //console.log(event.target.value);
        //React.useEffect(()=>{
            async function obtenerFaltas(){
                const data = await fetch(`${urlserviciosweb}falta/tipofalta/${event.target.value}`);
                if(data.status===200)
                {
                    const lista = await data.json();         
                    SetListaFalta(lista.data); 
                }
            }
            obtenerFaltas();       
        //},[]);

    };
      

    //----------------Obtener Valores de Formulario Registro de Parte--------------
    const {register, errors, handleSubmit} = useForm();
    const [entradas, setentradas] = React.useState([]);

    const procesarFormularioFaltaParte = (data, e) => {
        console.log("OBT DATOS FALTAPARTE");
        console.log(data);
        setentradas([
          ...entradas,
          data
        ])
        // limpiar campos        
        registrarFaltaParte(data);
        e.target.reset();        
    };


    async function registrarFaltaParte(datosFormulario){      

        const respuesta = await fetch(`${urlserviciosweb}faltaparte`,{
            method:'POST',
            body:JSON.stringify(datosFormulario),
            headers:{
              'Content-Type': 'application/json; charset=utf-8',
            },           
        }); 

        const mensaje = await respuesta.json();
        alert(mensaje.message);
        
        document.getElementById("formulariofaltaparte").reset();

    }    



    return (   

        <>
        <CRow>
            <CCol>
                <CCard>
                    <CCardHeader>
                        <strong>DETALLE DE LAS FALTAS DE LA PARTE</strong>
                    </CCardHeader>
                    <CCardBody>
        
                        <CRow>
                            <CCol sm="6">
                                <CCallout color="danger">
                                    <small className="text-muted">Nombre de la Parte Denunciada.</small>
                                    <br />
                                    <strong className="h6">{nombreparte}</strong>
                                </CCallout>
                            </CCol>
                        </CRow>

                        <CRow>
                            <CCol sm="12">
                            <FaltaParteList idparte={idparte}></FaltaParteList>  
                            </CCol>
                        </CRow>

                        <CRow>
                            <CCol xs="12" md="12">
                                <CCard borderColor="success">

                                    <CCardHeader>          
                                        <strong>FORMULARIO DE REGISTRO DE FALTAS DE PARTE DENUNCIADO</strong>
                                    </CCardHeader>

                                    <form  className="form-horizontal" id="formulariofaltaparte" onSubmit={handleSubmit(procesarFormularioFaltaParte)}  >
                                            
                                        <CCardBody>  

                                            <input 
                                                type="hidden"
                                                id="idparte" 
                                                name="idparte" 
                                                value={idparte} 
                                                ref={register({})}
                                            />                                                                                             

                                            <CFormGroup row> 
                                                <CCol md="3">
                                                    <CLabel className="col-sm-10 col-form-label"><strong>Seleccione el Tipo de la Falta:<code>(*)</code></strong></CLabel>
                                                </CCol>
                                                <CCol xs="12" md="7">  
                                                    <select 
                                                    //custom 
                                                    name="idtipofalta" 
                                                    id="idtipofalta"  
                                                    className="form-control my-2"
                                                    //onChange={e => SetIdentificadorTipoFalta(e.target.value)} 
                                                    onChange={handleChange}
                                                    >
                                                    <option value="0">Seleccionar el tipo de la falta.....</option>
                                                    {listatipofalta.map((element) => (        
                                                        <option key={ element.idtipofalta} value={ element.idtipofalta}>{ element.descripcion}</option>
                                                    ))} 
                                                    </select>
                                                    <CFormText>Seleccione el tipo de la falta.</CFormText>
                                                </CCol> 
                                            </CFormGroup> 

                                
                                            <CFormGroup row> 
                                                <CCol md="3">
                                                    <CLabel className="col-sm-10 col-form-label"><strong>Seleccione la Falta:<code>(*)</code></strong></CLabel>
                                                </CCol>
                                                <CCol xs="12" md="7">  
                                                    <select 
                                                    //custom 
                                                    name="idfalta" 
                                                    id="idfalta"  
                                                    className="form-control my-2"
                                                    ref={register({})}
                                                    >
                                                    <option value="0">Seleccionar la falta.....</option>    
                                                    {listafalta.length ? (listafalta.map((element) => (        
                                                        <option key={ element.idfalta} value={ element.idfalta}>{ element.descripcion}</option>
                                                    ))):("sin parametros")}                                                
                                                    </select>
                                                    <CFormText>Seleccione la falta.</CFormText>
                                                </CCol> 
                                            </CFormGroup>     

                                            <CFormGroup row> 
                                                <CCol md="3">
                                                    <CLabel className="col-sm-10 col-form-label"><strong>Descripcion de la falta:<code>(*)</code></strong></CLabel>
                                                </CCol>
                                                <CCol xs="12" md="7">  
                                                    <textarea
                                                        //className="form-control"
                                                        name="descripciondenuncia" 
                                                        id="descripciondenuncia"  
                                                        className="form-control my-2"                                                
                                                        rows="5"
                                                        ref={
                                                            register({                                                        
                                                                pattern:  { value: /^[.,0-9A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/i, message: 'Debe introducir solo letras y números'}
                                                            })
                                                        } 
                                                    />
                                                    <CFormText>Introduzca la descripcion de la denuncia.</CFormText>
                                                    <span className="text-danger text-small d-block mb-2">
                                                        {errors?.descripciondenuncia?.message}
                                                    </span>
                                                </CCol> 
                                            </CFormGroup>                              
                            
                                            <CCardFooter>
                                                <CButton type="submit"  size="sm" color="success" ><CIcon name="cil-save" /> Guardar</CButton>{ "   "}  
                                                <CButton size="sm" color="danger" to= {`/expediente/detalle/${identificadorexpediente}`} ><CIcon name="cil-chevron-left" /> Volver</CButton>  
                                            </CCardFooter>
                                                                                        
                                        </CCardBody>
                                    </form>
                                </CCard>
                            </CCol>  
                        </CRow>
                    </CCardBody>
                </CCard>
            </CCol>
        </CRow>        
        </>
    )

}

    
export default FaltaParteForm;