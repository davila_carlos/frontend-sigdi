const rol = {
  admin: 1,
  plataforma: 2,
  auxiliar: 3,
  secretario: 4,
  juez: 5,
  parte: 6,
};

export default rol;
