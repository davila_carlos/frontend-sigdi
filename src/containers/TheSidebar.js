import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
} from "@coreui/react";

import CIcon from "@coreui/icons-react";

// sidebar nav config
import navigation from "./_nav";

const TheSidebar = () => {
  const dispatch = useDispatch();
  const show = useSelector((state) => state.sidebarShow);

  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({ type: "set", sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/dashboard">
        <CIcon
          className="c-sidebar-brand-full"
          // name="logo-negative"
          src="./logo_sigdi.N.fw.png"
          height={50}
        />
        <CIcon
          className="c-sidebar-brand-minimized"
          // name="sygnet"
          src="./logo_sigdi.N.fw.png"
          height={35}
        />
      </CSidebarBrand>

      <CSidebarNav>
        <CCreateElement items={navigation} />
      </CSidebarNav>
    </CSidebar>
  );
};

export default React.memo(TheSidebar);
