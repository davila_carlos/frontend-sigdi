import ProtectedSidebarNavDropdown from "../components/ProtectedSidebarNavDropdown";
import ProtectedSidebarNavItem from "../components/ProtectedSidebarNavItem";

import rol from "../rol";

const fields = [
  {
    _tag: ProtectedSidebarNavItem,
    name: "Principal",
    to: "/dashboard",
    icon: "cil-speedometer",
    badge: {
      color: "info",
      //text: 'NEW',
    },
  },
  // {
  //   _tag: CSidebarNavTitle",
  //   _children: ["Administración"],
  // },
  {
    _tag: ProtectedSidebarNavDropdown,
    name: "Gestión de Usuarios",
    roles: [rol.admin],
    route: "/pages",
    icon: "cil-user",
    _children: [
      {
        _tag: ProtectedSidebarNavItem,
        name: "Registrar Usuario",
        roles: [rol.admin],
        to: "/usuarios/registro",
      },
      {
        _tag: ProtectedSidebarNavItem,
        name: "Lista de usuarios",
        roles: [rol.admin],
        to: "/usuarios",
      },
      {
        _tag: ProtectedSidebarNavItem,
        name: "Baja de Usuarios",
        roles: [rol.admin],
        to: "/usuarios",
      },
    ],
  },
  {
    _tag: ProtectedSidebarNavDropdown,
    name: "Catalogos",
    roles: [rol.admin],
    route: "/catalogos",
    icon: "cil-list",
    _children: [
      {
        _tag: ProtectedSidebarNavItem,
        name: "Unidades",
        roles: [rol.admin],
        to: "/",
      },
      {
        _tag: ProtectedSidebarNavItem,
        name: "Cargos",
        roles: [rol.admin],
        to: "/",
      },
      {
        _tag: ProtectedSidebarNavItem,
        name: "Tipo actuado",
        roles: [rol.admin],
        to: "/",
      },
    ],
  },
  {
    _tag: ProtectedSidebarNavItem,
    name: "Expedientes",
    icon: "cil-warning",
    roles: [rol.plataforma, rol.secretario, rol.auxiliar, rol.juez],
    to: "/expediente/expedientelist",
  },
  {
    _tag: ProtectedSidebarNavItem,
    name: "Registro parte",
    icon: "cil-user",
    roles: [rol.plataforma, rol.secretario],
  },
  {
    _tag: ProtectedSidebarNavItem,
    name: "Listado de expedientes",
    icon: "cil-list",
    roles: [rol.parte],
    to: "/expediente/falta/list",
  },
];

export default fields;
